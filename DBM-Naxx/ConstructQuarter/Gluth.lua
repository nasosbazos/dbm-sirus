local mod = DBM:NewMod("Gluth", "DBM-Naxx", 2)
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 2869 $"):sub(12, -3))
mod:SetCreatureID(15932)

mod:RegisterCombat("combat")

mod:EnableModel()

mod:RegisterEvents(
    "SPELL_DAMAGE",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE"
)


local warnDecimateSoon    = mod:NewSoonAnnounce(54426, 2)
local warnDecimateNow     = mod:NewSpellAnnounce(54426, 3)
local warnEnrage          = mod:NewSpellAnnounce(54427, 3)
local warnMortalWound     = mod:NewAnnounce("WarnMortalWound", 4, 54378)

local enrageTimer         = mod:NewBerserkTimer(420)
local timerDecimate       = mod:NewCDTimer(114, 54426)
local timerMortalWound    = mod:NewCDTimer(10, 54378)
local timerEnrage         = mod:NewCDTimer(20, 54427)

function mod:OnCombatStart(delay)
    enrageTimer:Start(420 - delay)
    timerDecimate:Start(- delay)
    warnDecimateSoon:Schedule(109 - delay)
    timerMortalWound:Start()
    timerEnrage:Start()
end

local decimateSpam = 0
function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(28375) and (GetTime() - decimateSpam) > 20 then
        decimateSpam = GetTime()
        warnDecimateNow:Show()
        timerDecimate:Start()
        warnDecimateSoon:Schedule(109)
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(54427) then
        timerEnrage:Start()
        warnEnrage:Show()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(54378) then
        timerMortalWound:Start()
        warnMortalWound:Show(args.spellName, args.destName, args.amount or 1)
    end
end
mod.SPELL_AURA_APPLIED_DOSE = mod.SPELL_AURA_APPLIED
