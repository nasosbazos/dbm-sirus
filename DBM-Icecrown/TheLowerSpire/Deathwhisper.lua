local mod = DBM:NewMod("Deathwhisper", "DBM-Icecrown", 1)
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4836 $"):sub(12, -3))
mod:SetCreatureID(36855)
mod:SetUsedIcons(4, 5, 6, 7, 8)
mod:RegisterCombat("yell", L.YellPull)

mod:RegisterEvents(
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REFRESH",
    "SPELL_MISSED",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_REMOVED",
    "SPELL_CAST_START",
    "SPELL_SUMMON",
    "SWING_DAMAGE",
    "CHAT_MSG_MONSTER_YELL",
    "UNIT_TARGET"
)

local canPurge = select(2, UnitClass("player")) == "MAGE"
              or select(2, UnitClass("player")) == "SHAMAN"
              or select(2, UnitClass("player")) == "PRIEST"

local warnAddsSoon                     = mod:NewAnnounce("WarnAddsSoon", 2)
local warnDominateMind                 = mod:NewTargetAnnounce(71289, 3)
local warnDeathDecay                   = mod:NewSpellAnnounce(72108, 2)
local warnSummonSpirit                 = mod:NewSpellAnnounce(71426, 2)
local warnReanimating                  = mod:NewAnnounce("WarnReanimating", 3)
local warnDarkTransformation           = mod:NewSpellAnnounce(70900, 4)
local warnDarkEmpowerment              = mod:NewSpellAnnounce(70901, 4)
local warnPhase2                       = mod:NewPhaseAnnounce(2, 1)    
local warnFrostbolt                    = mod:NewCastAnnounce(72007, 2)
local warnTouchInsignificance          = mod:NewAnnounce("WarnTouchInsignificance", 2, 71204, mod:IsTank() or mod:IsHealer())
local warnDarkMartyrdom                = mod:NewSpellAnnounce(72499, 4)
local warnVengefulShade                = mod:NewTargetAnnounce(71426, 3)

local specWarnCurseTorpor              = mod:NewSpecialWarningYou(71237)
local specWarnDeathDecay               = mod:NewSpecialWarningMove(72108)
local specWarnVampricMight             = mod:NewSpecialWarningDispel(70674, canPurge)
local specWarnDarkMartyrdom            = mod:NewSpecialWarningMove(72499, mod:IsMelee())
local specWarnVengefulShade            = mod:NewSpecialWarningYou(71426)

local yellVengefulShade                = mod:NewYell(71426)

local timerAdds                        = mod:NewTimer(60, "TimerAdds", 61131)
local timerDominateMind                = mod:NewBuffActiveTimer(12, 71289)
local timerDominateMindCD              = mod:NewCDTimer(40, 71289)
local timerSummonSpiritCD              = mod:NewCDTimer(10, 71426, nil, false)
local timerFrostboltVolleyCD           = mod:NewCDTimer(20, 72906)
local timerTouchInsignificance         = mod:NewTargetTimer(30, 71204, nil, mod:IsTank() or mod:IsHealer())
local timerTouchInsignificanceCD       = mod:NewCDTimer(6, 71204)

local berserkTimer                     = mod:NewBerserkTimer(600)

mod:AddSetIconOption("SetIconOnDeformedFanatic", nil , {8}, true)
mod:AddSetIconOption("SetIconOnEmpoweredAdherent", nil , {7}, false)
mod:AddSetIconOption("SetIconOnDominateMind", 71289, {6,5,4}, true)
mod:AddBoolOption("ShieldHealthFrame", true, "misc")
mod:AddBoolOption("AnnounceVengefulShadeTargets", true)
mod:AddBoolOption("RemoveWeaponOnMindControl", true)
mod:AddBoolOption("RemoveShadowResistanceBuffs", true)
mod:RemoveOption("HealthFrame")


local dominateMindTargets    = {}
local dominateMindIcon     = 6
local deformedFanatic
local empoweredAdherent

function mod:RemoveBuffs()
    CancelUnitBuff("player", (GetSpellInfo(48469)))        -- Mark of the Wild
    CancelUnitBuff("player", (GetSpellInfo(48470)))        -- Gift of the Wild
    CancelUnitBuff("player", (GetSpellInfo(48169)))        -- Shadow Protection
    CancelUnitBuff("player", (GetSpellInfo(48170)))        -- Prayer of Shadow Protection
end

function mod:showDominateMindWarning()
    warnDominateMind:Show(table.concat(dominateMindTargets, "<, >"))
    timerDominateMind:Start()
    timerDominateMindCD:Start()
    self:ScheduleMethod(37, "PlaySound", "disarm")
    table.wipe(dominateMindTargets)
    dominateMindIcon = 6
end

function mod:OnCombatStart(delay)
    if self.Options.ShieldHealthFrame then
        DBM.BossHealth:Show(L.name)
        DBM.BossHealth:AddBoss(36855, L.name)
        self:ScheduleMethod(0.5, "CreateShildHPFrame")
    end        
    if self.Options.RemoveShadowResistanceBuffs then
        mod:ScheduleMethod(0.1, "RemoveBuffs")
    end
    berserkTimer:Start(-delay)
    timerAdds:Start(6)
    warnAddsSoon:Schedule(4)            -- 3sec pre-warning on start
    self:ScheduleMethod(6, "addsTimer")
    if not self:IsDifficulty("normal10") then
        timerDominateMindCD:Start(30)        -- Sometimes 1 fails at the start, then the next will be applied 70 secs after start ?? :S
    end
    table.wipe(dominateMindTargets)
    dominateMindIcon = 6
    deformedFanatic = nil
    empoweredAdherent = nil
end

do    -- add the additional Shield Bar
    local last = 100
    local function getShieldPercent()
        local guid = UnitGUID("focus")
        if mod:GetCIDFromGUID(guid) == 36855 then 
            last = math.floor(UnitMana("focus")/UnitManaMax("focus") * 100)
            return last
        end
        for i = 0, GetNumRaidMembers(), 1 do
            local unitId = ((i == 0) and "target") or "raid"..i.."target"
            local guid = UnitGUID(unitId)
            if mod:GetCIDFromGUID(guid) == 36855 then
                last = math.floor(UnitMana(unitId)/UnitManaMax(unitId) * 100)
                return last
            end
        end
        return last
    end
    function mod:CreateShildHPFrame()
        DBM.BossHealth:AddBoss(getShieldPercent, L.ShieldPercent)
    end
end

function mod:addsTimer()
    timerAdds:Cancel()
    warnAddsSoon:Cancel()
    if self:IsDifficulty("heroic10", "heroic25") then
        self:PlaySound("FBI","enemy_spot","ispancy")                                 -- FBI open up!
        warnAddsSoon:Schedule(40)    -- 5 secs prewarning
        self:ScheduleMethod(45, "addsTimer")
        timerAdds:Start(45)
    else
        self:PlaySound("FBI","enemy_spot","ispancy")                                 -- FBI open up!
        warnAddsSoon:Schedule(55)    -- 5 secs prewarning
        self:ScheduleMethod(60, "addsTimer")
        timerAdds:Start()
    end
end

function mod:TrySetTarget()
    if DBM:GetRaidRank() >= 1 then
        for i = 1, GetNumRaidMembers() do
            if UnitGUID("raid"..i.."target") == deformedFanatic then
                deformedFanatic = nil
                SetRaidTarget("raid"..i.."target", 8)
            elseif UnitGUID("raid"..i.."target") == empoweredAdherent then
                empoweredAdherent = nil
                SetRaidTarget("raid"..i.."target", 7)
            end
            if not (deformedFanatic or empoweredAdherent) then
                break
            end
        end
    end
end
    
function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(71001, 72108, 72109, 72110) then
        if args:IsPlayer() then
            specWarnDeathDecay:Show()
        end
        if self:AntiSpam(5, "decay")then
            warnDeathDecay:Show()
        end
    elseif args:IsSpellID(71237) and args:IsPlayer() then
        specWarnCurseTorpor:Show()
    elseif args:IsSpellID(70674) and not args:IsDestTypePlayer() and (UnitName("target") == L.Fanatic1 or UnitName("target") == L.Fanatic2 or UnitName("target") == L.Fanatic3) then
        specWarnVampricMight:Show(args.destName)
    elseif args:IsSpellID(71204) then
        warnTouchInsignificance:Show(args.spellName, args.destName, args.amount or 1)
        timerTouchInsignificance:Start(args.destName)
        timerTouchInsignificanceCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(71204) then
        warnTouchInsignificance:Show(args.spellName, args.destName, args.amount or 1)
        timerTouchInsignificance:Start(args.destName)
        timerTouchInsignificanceCD:Start()
    end
end
mod.SPELL_AURA_REFRESH = mod.SPELL_AURA_APPLIED_DOSE

function mod:SPELL_MISSED(args)
    if args:IsSpellID(71204) then
        timerTouchInsignificanceCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(71289) then
        if args:IsPlayer() and self.Options.RemoveWeaponOnMindControl then
           if self:IsWeaponDependent() then
                PickupInventoryItem(16)
                PutItemInBackpack()
                PickupInventoryItem(17)
                PutItemInBackpack()
            elseif select(2, UnitClass("player")) == "HUNTER" then
                PickupInventoryItem(18)
                PutItemInBackpack()
            end
        end
        if args:IsPlayer() then
            self:ScheduleMethod(13, "PlaySound", "disarm_2")
        end
        dominateMindTargets[#dominateMindTargets + 1] = args.destName
        if self.Options.SetIconOnDominateMind then
            self:SetIcon(args.destName, dominateMindIcon, 12)
            dominateMindIcon = dominateMindIcon - 1
        end
        self:UnscheduleMethod("showDominateMindWarning")
        if self:IsDifficulty("heroic10", "normal25") or (self:IsDifficulty("heroic25") and #dominateMindTargets >= 3) then
            self:showDominateMindWarning()
        else
            self:ScheduleMethod(0.9, "showDominateMindWarning")
        end
    elseif args:IsSpellID(71204) then
        timerTouchInsignificanceCD:Start()
    elseif args:IsSpellID(72906) then
        timerFrostboltVolleyCD:Start()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(70842) then
        warnPhase2:Show()
        timerTouchInsignificanceCD:Start()
        timerFrostboltVolleyCD:Start()
        timerAdds:Cancel()
        warnAddsSoon:Cancel()
        self:UnscheduleMethod("addsTimer")
        if self:IsDifficulty("heroic10", "heroic25") then self:addsTimer() end
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(71420, 72007, 72501, 72502) then
        warnFrostbolt:Show()
    elseif args:IsSpellID(70900) then
        warnDarkTransformation:Show()
        if self.Options.SetIconOnDeformedFanatic then
            deformedFanatic = args.sourceGUID
            self:TrySetTarget()
        end
    elseif args:IsSpellID(70901) then
        warnDarkEmpowerment:Show()
        if self.Options.SetIconOnEmpoweredAdherent then
            empoweredAdherent = args.sourceGUID
            self:TrySetTarget()
        end
    elseif args:IsSpellID(72499, 72500, 72497, 72496) then
        warnDarkMartyrdom:Show()
        specWarnDarkMartyrdom:Show()
    end
end

local excludeTargets = {}

function mod:ShadeTargets()
    local shadeTargets = {}
    for i = 1, GetNumRaidMembers() do
        if UnitThreatSituation("raid" .. i) and UnitThreatSituation("raid" .. i) >= 2 and not excludeTargets[UnitName("raid" .. i) or ""] then
            shadeTargets[#shadeTargets + 1] = UnitName("raid" .. i)
            if UnitIsUnit("player", "raid" .. i) then
                self:PlaySound("surprise")
                specWarnVengefulShade:Show()
                yellVengefulShade:Yell()
            end
        end
    end
    if #shadeTargets > 0 then
        warnVengefulShade:Show(table.concat(shadeTargets, "<, >"))
    end
    table.wipe(shadeTargets)
    table.wipe(excludeTargets)
end

function mod:SPELL_SUMMON(args)
    if args:IsSpellID(71426) and self:AntiSpam(5, "summon") then
        warnSummonSpirit:Show()
        timerSummonSpiritCD:Start()
        if self.Options.AnnounceVengefulShadeTargets then
            for i = 1, GetNumRaidMembers() do
                if UnitThreatSituation("raid" .. i) and UnitThreatSituation("raid" .. i) >= 2 then
                    excludeTargets[UnitName("raid" .. i)] = true
                end
            end
            self:ScheduleMethod(0.2, "ShadeTargets")
        end
    end
end

function mod:SWING_DAMAGE(args)
    if args:IsPlayer() and args:GetSrcCreatureID() == 38222 then
        specWarnVengefulShade:Show()
    end
end

function mod:UNIT_TARGET()
    if empoweredAdherent or deformedFanatic then
        self:TrySetTarget()
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellReanimatedFanatic or msg:find(L.YellReanimatedFanatic) then
        warnReanimating:Show()
    end
end

function mod:OnCombatEnd()
    DBM.BossHealth:Clear()
end