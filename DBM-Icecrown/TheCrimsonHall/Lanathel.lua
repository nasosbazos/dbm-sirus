local mod = DBM:NewMod("Lanathel", "DBM-Icecrown", 3)
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4840 $"):sub(12, -3))
mod:SetCreatureID(37955)
mod:SetUsedIcons(4, 5, 6, 7, 8)

mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_CAST_SUCCESS",
    "SPELL_DAMAGE",
    "SPELL_PERIODIC_DAMAGE",
    "CHAT_MSG_RAID_BOSS_EMOTE"
)

local warnPactDarkfallen             = mod:NewTargetAnnounce(71340, 4)
local warnBloodMirror                = mod:NewTargetAnnounce(71510, 3, nil, mod:IsTank() or mod:IsHealer())
local warnSwarmingShadows            = mod:NewTargetAnnounce(71266, 4)
local warnInciteTerror               = mod:NewSpellAnnounce(73070, 3)
local warnVampricBite                = mod:NewTargetAnnounce(71727, 2)
local warnMindControlled             = mod:NewTargetAnnounce(70923, 4)
local warnBloodthirstSoon            = mod:NewSoonAnnounce(71474, 2)
local warnBloodthirst                = mod:NewTargetAnnounce(71474, 3, nil, false)
local warnEssenceoftheBloodQueen     = mod:NewTargetAnnounce(71473, 3, nil, false)

local specWarnBloodBolt              = mod:NewSpecialWarningSpell(71772)
local specWarnPactDarkfallen         = mod:NewSpecialWarningYou(71340)
local specWarnEssenceoftheBloodQueen = mod:NewSpecialWarningYou(71473)
local specWarnBloodthirst            = mod:NewSpecialWarningYou(71474)
local specWarnSwarmingShadows        = mod:NewSpecialWarningMove(71266)
local specWarnMindConrolled          = mod:NewSpecialWarningTarget(70923, mod:IsTank())

local yellFrenzy                     = mod:NewYell(70877)

local timerNextInciteTerror          = mod:NewNextTimer(103, 73070)
local timerFirstBite                 = mod:NewCastTimer(15, 71727)
local timerNextPactDarkfallen        = mod:NewNextTimer(30.5, 71340)
local timerNextSwarmingShadows       = mod:NewNextTimer(30.5, 71266)
local timerInciteTerror              = mod:NewBuffActiveTimer(4, 73070)
local timerBloodBolt                 = mod:NewBuffActiveTimer(6, 71772)
local timerBloodThirst               = mod:NewBuffActiveTimer(10, 71474)
local timerEssenceoftheBloodQueen    = mod:NewBuffActiveTimer(60, 71473)

local berserkTimer                    = mod:NewBerserkTimer(330)


mod:AddSetIconOption("SwarmingShadowsIcon", 71266, {8}, true)
mod:AddSetIconOption("BloodMirrorIcon", 71510, {7}, false)
mod:AddSetIconOption("SetIconOnDarkFallen", 71340, {6,5,4}, true)
mod:AddBoolOption("NextBiteArrow", true)
mod:AddBoolOption("RangeFrame", true)

local pactTargets = {}
local pactIcons = 6
local firstBite = GetTime()
local pList = LQ and LQ.Options.priorityList
local cBites = {}
local isDiff10

local function checkEntry(t, val)
    for i, v in ipairs(t) do
        if v == val then
            return i
        end
    end
    return false
end

local function warnPactTargets()
    warnPactDarkfallen:Show(table.concat(pactTargets, "<, >"))
    table.wipe(pactTargets)
    timerNextPactDarkfallen:Start(30)
    pactIcons = 6
end

function mod:OnCombatStart(delay)
    berserkTimer:Start(-delay)
    timerFirstBite:Start(-delay)
    timerNextPactDarkfallen:Start(15-delay)
    timerNextSwarmingShadows:Start(-delay)
    table.wipe(pactTargets)
    table.wipe(cBites)
    pactIcons = 6
    firstBite = GetTime()
    isDiff10 = self:IsDifficulty("normal10", "heroic10")
    if self.Options.RangeFrame then
        DBM.RangeCheck:Show(8)
    end
    self:ScheduleMethod(isDiff10 and (122-delay) or (125-delay), "PlaySound", "djeep")
    timerNextInciteTerror:Start(isDiff10 and (124-delay) or (131-delay))
end

function mod:OnCombatEnd()
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
    if self.Options.NextBiteArrow then
        DBM.Arrow:Hide()
    end
end

function mod:OnSync(msg, arg)
    if self.Options.NextBiteArrow and msg == "NextBite" then
        local src, tar = strsplit(":", arg)
        if src == UnitName("player") then
            DBM.Arrow:ShowRunTo(tar)
        end
    end
end

function mod:UpdateBites()
    local s = math.floor((args.timestamp - firstBite)/(isDiff10 and 75 or 60))
    for k,v in pairs(cBites) do
        local i = checkEntry(pList, k)
        if i and (s < (isDiff10 and 3 or 4)) and v ~= pList[i+(2^s)] then
            cBites[k] = pList[i+(2^s)]
            self:SendSync("NextBite", k .. ":" .. pList[i+(2^s)])
        end
    end
    self:ScheduleMethod(5, "UpdateBites")
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(71340) then        --Pact of the Darkfallen
        pactTargets[#pactTargets + 1] = args.destName
        if args:IsPlayer() then
            specWarnPactDarkfallen:Show()
        end
        if self.Options.SetIconOnDarkFallen then--Debuff doesn't actually last 30 seconds
            self:SetIcon(args.destName, pactIcons, 28)--it lasts forever, but if you still have it after 28 seconds
            pactIcons = pactIcons - 1--then you're probably dead anyways
        end
        self:Unschedule(warnPactTargets)
        if #pactTargets >= 3 then
            warnPactTargets()
        else
            self:Schedule(0.3, warnPactTargets)
        end
    elseif args:IsSpellID(71510, 70838) then
        warnBloodMirror:Show(args.destName)
        if self.Options.BloodMirrorIcon then
            self:SetIcon(args.destName, 7)
        end
    elseif args:IsSpellID(70877, 71474) then
        warnBloodthirst:Show(args.destName)
        if args:IsPlayer() then
            specWarnBloodthirst:Show()
            yellFrenzy:Yell()
            timerBloodThirst:Start(isDiff10 and 15)
            if self.Options.NextBiteArrow and pList and DBM:GetRaidRank() == 2 then
                self:UnscheduleMethod("UpdateBites")
            end
        end
    elseif args:IsSpellID(70867, 70879, 71473, 71525) or args:IsSpellID(71530, 71531, 71532, 71533) then    --Essence of the Blood Queen
        warnEssenceoftheBloodQueen:Show(args.destName)
        if args:IsPlayer() then
            specWarnEssenceoftheBloodQueen:Show()
            self:PlaySound("welcome")
        end
        timerEssenceoftheBloodQueen:Start(isDiff10 and 75)
        if not args:IsSrcTypePlayer() then
            firstBite = args.timestamp
        end
        if self.Options.NextBiteArrow and pList and DBM:GetRaidRank() == 2 then
            local s = math.floor((args.timestamp - firstBite)/(isDiff10 and 75 or 60))
            local i = checkEntry(pList, args.destName)
            self:UnscheduleMethod("UpdateBites")
            if i and (s < (isDiff10 and 3 or 4)) then
                self:SendSync("NextBite", args.destName .. ":" .. pList[i+(2^s)])
                cBites[args.destName] = pList[i+(2^s)]
                self:ScheduleMethod(9, "UpdateBites")
            end
        end
    elseif args:IsSpellID(70923) then
        if args:IsPlayer() then
            self:PlaySound("ohshit")                  -- "oh shit, i'm sorry"
        end
        warnMindControlled:Show(args.destName)
        specWarnMindConrolled:Show(args.destName)
    elseif args:IsSpellID(71772) then
        specWarnBloodBolt:Show()
        timerBloodBolt:Start()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(71340) then                --Pact of the Darkfallen
        if self.Options.SetIconOnDarkFallen then
            self:SetIcon(args.destName, 0)        --Clear icon once you got to where you are supposed to be
        end
    elseif args:IsSpellID(71510, 70838) then    --Blood Mirror
        if self.Options.BloodMirrorIcon then
            self:SetIcon(args.destName, 0)
        end
    elseif args:IsSpellID(70877, 71474) then
        if args:IsPlayer() then
            timerBloodThirst:Cancel()
        end
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(73070) then                --Incite Terror (fear before air phase)
        warnInciteTerror:Show()
        timerInciteTerror:Start()
        timerNextSwarmingShadows:Start()
        timerNextPactDarkfallen:Start(25)
        self:ScheduleMethod(isDiff10 and 120 or 100, "PlaySound", "djeep")
        timerNextInciteTerror:Start(isDiff10 and 120)
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(71726, 71727, 71728, 71729) and args:GetSrcCreatureID() == 37955 then    -- Vampric Bite (first bite only, hers)
        warnVampricBite:Show(args.destName)
    end
end

function mod:SPELL_PERIODIC_DAMAGE(args)
    if args:IsPlayer() and args:IsSpellID(71277, 72638, 72639, 72640) and self:AntiSpam(3, "swarm") then        --Swarn of Shadows (spell damage, you're standing in it.)
        specWarnSwarmingShadows:Show()
    end
end

function mod:CHAT_MSG_RAID_BOSS_EMOTE(msg, _, _, _, target)
    if msg:match(L.SwarmingShadows) then
        warnSwarmingShadows:Show(target)
        timerNextSwarmingShadows:Start()
        if target == UnitName("player") then
            self:PlaySound("fear2")
            specWarnSwarmingShadows:Show()
        end
        if self.Options.SwarmingShadowsIcon then
            self:SetIcon(target, 8, 6)
        end
    end
end
