local mod = DBM:NewMod("Janalai", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4855 $"):sub(12, -3))

mod:SetCreatureID(23578)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED_DOSE",
    "CHAT_MSG_MONSTER_YELL"
)

local warnHatchersSoon      = mod:NewAnnounce("WarnHatchersSoon", 1, 26297)
local warnHatchers          = mod:NewAnnounce("WarnHatchers", 2, 26297)
local warnBombs             = mod:NewSpellAnnounce(42621, 3)
local warnFlameBuffet       = mod:NewAnnounce("WarnFlameBuffet", 4, 43299)

local timerHatchers         = mod:NewTimer(155, "TimerHatchers", 26297)
local timerBombsCD          = mod:NewCDTimer(40, 42621)
local timerExplosion        = mod:NewTimer(11, "TimerExplosion", 66313)
local timerBreathCD         = mod:NewCDTimer(12, 43140)

function mod:OnCombatStart(delay)
    warnHatchersSoon:Schedule(9-delay)
    timerHatchers:Start(14-delay)
    timerBombsCD:Start(40-delay)
    timerBreathCD:Start(-delay)
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(43140) then
        timerBreathCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(43299) then
        if args.amount >= 10 and self:AntiSpam(3, "buffet") then
            warnFlameBuffet:Show(args.spellName, args.destName, args.amount)
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellBombs then
        self:PlaySound("bomb_p", "bomba", "t_zaminirovali", "strashno")
        self:ScheduleMethod(10, "PlaySound", "wtf_boom" )
        warnBombs:Show()
        timerBreathCD:Extend(10)
        timerExplosion:Start()
        timerBombsCD:Start()
    elseif msg == L.YellHatcher then
        self:PlaySound("ispancy")
        warnHatchersSoon:Schedule(145)
        warnHatchers:Show()
        timerHatchers:Start()
    end
end
