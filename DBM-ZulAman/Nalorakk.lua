local mod = DBM:NewMod("Nalorakk", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4853 $"):sub(12, -3))

mod:SetCreatureID(23576)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REFRESH",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_YELL"
)

local warnBearFormSoon      = mod:NewAnnounce("WarnBearFormSoon", 2, 9634)
local warnTrollFormSoon     = mod:NewAnnounce("WarnTrollFormSoon", 1, 26297)
local warnSilence           = mod:NewSpellAnnounce(42398, 2)
local warnRend              = mod:NewTargetAnnounce(42397, 3)
local warnMangle            = mod:NewTargetAnnounce(42389, 4)
local warnSurge             = mod:NewSpellAnnounce(42402, 2)

local timerNextBearForm     = mod:NewTimer(45, "BearForm", 9634)
local timerNextTrollForm    = mod:NewTimer(30, "TrollForm", 26297)
local timerSilenceCD        = mod:NewCDTimer(20, 42398)
local timerRendCD           = mod:NewCDTimer(10, 42397)
local timerRend             = mod:NewTargetTimer(5, 42397)
local timerSurgeCD          = mod:NewCDTimer(20, 42402)
local timerSwipeCD          = mod:NewCDTimer(10, 42384)
local timerMangleCD         = mod:NewCDTimer(30, 42389)
local timerMangle           = mod:NewTargetTimer(60, 42389)

local berserkTimer          = mod:NewBerserkTimer(600)

mod:AddBoolOption("AutomaticallyRemoveBoP", mod:IsTank())

function mod:PreWarnBearForm()
    self:PlaySound("b_nelez")
    warnBearFormSoon:Show()
    timerSilenceCD:Start(6)
    timerRendCD:Start(7)
end

function mod:OnCombatStart(delay)
    self:ScheduleMethod(40-delay, "PreWarnBearForm")
    timerSurgeCD:Start(-delay)
    timerSwipeCD:Start(-delay)
    timerMangleCD:Start(5-delay)
    timerNextBearForm:Start(-delay)
    berserkTimer:Start(-delay)
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(42398) then
        self:PlaySound("b_bearform", "b_kakebnut")
        warnSilence:Show()
        timerSilenceCD:Start()
    elseif args:IsSpellID(42397) then
        timerRendCD:Start()
    elseif args:IsSpellID(42389) then
        timerMangleCD:Start()
    elseif args:IsSpellID(42402) then
        warnSurge:Show()
        timerSurgeCD:Start()
    elseif args:IsSpellID(42384) then
        timerSwipeCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(42389) then
        warnRend:Show(args.destName)
        timerRend:Start(args.destName)
    elseif args:IsSpellID(42389) then
        warnMangle:Show(args.destName)
        timerMangle:Start(args.destName)
    elseif args:IsSpellID(10278) then
        if args:IsPlayer() and self.Options.AutomaticallyRemoveBoP then
            CancelUnitBuff("player", (GetSpellInfo(10278)))
        end
    end
end

function mod:SPELL_AURA_REFRESH(args)
    if args:IsSpellID(42389) then
        warnMangle:Show(args.destName)
        timerMangle:Start(args.destName)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(42389) then
        timerMangle:Cancel(args.destName)
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellBear then 
        self:PlaySound("b_palku")
        warnTrollFormSoon:Schedule(25)
        timerSurgeCD:Cancel()
        timerSwipeCD:Cancel()
        timerMangleCD:Cancel()
        timerSilenceCD:Start()
        timerNextTrollForm:Start()
    elseif msg == L.YellTroll then
        self:ScheduleMethod(40, "PreWarnBearForm")
        timerSilenceCD:Cancel()
        timerRendCD:Cancel()
        timerSurgeCD:Start()
        timerNextBearForm:Start()
    end
end
