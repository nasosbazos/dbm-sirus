local mod = DBM:NewMod("Akilzon", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4857 $"):sub(12, -3))

mod:SetCreatureID(23574)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED"
)

local warnStormSoon         = mod:NewSoonAnnounce(43648, 3)
local warnStorm             = mod:NewTargetAnnounce(43648, 4)
local warnDisrupt           = mod:NewTargetAnnounce(43622, 2)
local warnWind              = mod:NewTargetAnnounce(43621, 1)

local specWarnDisrupt       = mod:NewSpecialWarningYou(44008)

local yellStorm             = mod:NewYell(43648)

local timerStormCD          = mod:NewCDTimer(60, 43648)
local timerStorm            = mod:NewCastTimer(8, 43648)
local timerDisruptCD        = mod:NewCDTimer(15, 43622)
local timerDisrupt          = mod:NewTargetTimer(20, 44008)
local timerWindCD           = mod:NewCDTimer(27, 43621)

local berserkTimer          = mod:NewBerserkTimer(600)

mod:AddSetIconOption("SetIconOnElectricStorm", 43648, {8}, true)
mod:AddSetIconOption("SetIconOnWind", 43621, {7}, true)
mod:AddBoolOption("RangeFrame", true)

function mod:OnCombatStart(delay)
    warnStormSoon:Schedule(55-delay)
    timerStormCD:Start(-delay)
    timerDisruptCD:Start(10-delay)
    berserkTimer:Start(-delay)
    if self.Options.RangeFrame then
        DBM.RangeCheck:Show(12)
    end
end

function mod:DisruptTarget()
    local target = self:GetBossTarget(23574)
    if not target then return end
    if target == UnitName("player") then 
        self:PlaySound("run")
        self:AntiSpam(2,"disrupt")
        specWarnDisrupt:Show()
    end
    warnDisrupt:Show(target)
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(43622) then
        self:ScheduleMethod(0.1, "DisruptTarget")
        timerDisruptCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(43621) then
        if self.Options.SetIconOnWind then
            self:SetIcon(args.destName, 7)
        end
        if args:IsPlayer() then
            self:PlaySound("ebany_rot")
        end
        warnWind:Show(args.destName)
        timerWindCD:Start()
    end
end


function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(43648) then
        if self.Options.SetIconOnElectricStorm then
            self:SetIcon(args.destName, 8)
        end
        warnStormSoon:Schedule(55)
        warnStorm:Show(args.destName)
        if args:IsPlayer() then
            yellStorm:Yell()
        end
        timerStormCD:Start()
        timerStorm:Start()
    elseif args:IsSpellID(44008) and args:IsPlayer() then
        if self:AntiSpam(2,"disrupt") then
            specWarnDisrupt:Show()
        end
        timerDisrupt:Start(args.destName)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(43648) then
        self:RemoveIcon(args.destName)
    end
end

function mod:OnCombatEnd()
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
end