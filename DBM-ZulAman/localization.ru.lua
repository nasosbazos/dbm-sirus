﻿if GetLocale() ~= "ruRU" then return end

local L

--Nalorakk
L = DBM:GetModLocalization("Nalorakk")

L:SetGeneralLocalization{
    name = "Налоракк"
}

L:SetTimerLocalization{
    BearForm                = "Форма медведя",
    TrollForm               = "Форма тролля"
}

L:SetWarningLocalization{
    WarnBearFormSoon        = "Скоро форма медведя",
    WarnTrollFormSoon       = "Скоро форма тролля"
}

L:SetOptionLocalization{
    WarnBearFormSoon        = "Предупреждать заранее о форме медведя",
    WarnTrollFormSoon       = "Предупреждать заранее о форме тролля",
    BearForm                = "Отсчет времени до следующей формы медведя",
    TrollForm               = "Отсчет времени до следующей формы тролля",
    AutomaticallyRemoveBoP  = "Автоматически снимать $spell:10278"
}

L:SetMiscLocalization{
    YellPull                = "Очень скоро вы умрете!",
    YellKill                = "Я буду... ждать вас... в другом мире.",
    YellBear                = "Хотели разбудить во мне зверя? Вам это удалось.",
    YellTroll               = "C дороги!"
}

--Akilzon
L = DBM:GetModLocalization("Akilzon")

L:SetGeneralLocalization{
    name = "Акил'зон"
}

L:SetTimerLocalization{

}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
    RangeFrame              = "Показывать окно проверки дистанции (12 м)",
    SetIconOnElectricStorm  = "Отмечать на ком Электрическая буря",
    SayOnElectricStorm      = "Говорить в чат на ком Электрическая буря"
}

L:SetMiscLocalization{
    YellPull                = "Я – охотник! Вы – добыча!",
    YellKill                = "Вам не победить... мой дух..."
}

--Jan'alai
L = DBM:GetModLocalization("Janalai")

L:SetGeneralLocalization{
    name = "Джан'алай"
}

L:SetTimerLocalization{
    TimerHatchers           = "Вызов хранителей",
    TimerExplosion          = "Взрыв!"
}

L:SetWarningLocalization{
    WarnHatchers            = "Призыв хранителей",
    WarnHatchersSoon        = "Скоро призыв хранителей",
    WarnFlameBuffet         = "%s на |3-5(>%s<) (%s)"
}

L:SetOptionLocalization{
    WarnHatchers            = "Предупреждение о призыве хранителей",
    WarnHatchersSoon        = "Предупреждать заранее о призыве хранителей",
    WarnFlameBuffet         = "Предупреждение о количестве стаков $spell:43299",
    TimerHatchers           = "Отсчет времени до прихода хранителей",
    TimerBombs              = "Отсчет времени до начала установки бомб",
    TimerExplosion          = "Отсчет времени до взрыва"
}

L:SetMiscLocalization{
    YellPull                = "Духи ветра станут вашей погибелью!",
    YellKill                = "У Зулджина... есть сюрприз для вас...",
    YellBombs               = "Щас я вас сожгу!",
    YellHatcher             = "Эй, хранители! Займитесь яйцами!"
}

--Halazzi
L = DBM:GetModLocalization("Halazzi")

L:SetGeneralLocalization{
    name = "Халаззи"
}

L:SetTimerLocalization{
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
}

L:SetMiscLocalization{
}

--Malacrass
L = DBM:GetModLocalization("Malacrass")

L:SetGeneralLocalization{
    name = "Малакрасс"
}

L:SetTimerLocalization{
    TimerSpecial            = "Спец. способность %s"
}

L:SetWarningLocalization{
    WarnSiphon              = "Малакрасс крадет способности у >%s< ",
    WarnSpecial             = "%s",
    WarnSpecialOn           = "%s на >%s<",
    SpecWarnMove            = "%s отойдите!"
}

L:SetOptionLocalization{
    WarnSiphon              = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.spell:format(43501, GetSpellInfo(43501) or "unknown"),
    WarnSpecial             = "Предупреждать о применении спец-способностей",
    WarnSpecialOn           = "Предупреждать о применении спец-способностей к игрокам",
    SpecWarnMove            = "Спец-предупреждение для опасных АОЕ способностей",
    TimerSpecial            = "Отсчитывать время между спец-способностями"
}

L:SetMiscLocalization{
    YellPull                    = "Тьма поглотит вас!",
    YellKill                    = "Я не умру... так... просто..."
}

--ZulJin
L = DBM:GetModLocalization("ZulJin")

L:SetGeneralLocalization{
    name = "Зул'Джин"
}

L:SetTimerLocalization{
}

L:SetWarningLocalization{
    WarnNextPhaseSoon       = "Скоро фаза %s",
    WarnNextPhase           = "Фаза %s",
    SpecWarnFlamePillarNear = "Столб огня около вас - отойдите"
}

L:SetOptionLocalization{
    WarnNextPhaseSoon       = "Предупреждать о скорой смене облика",
    WarnNextPhase           = "Предупреждать о смене облика",
    SpecWarnFlamePillarNear = "Спец-предупреждение, когда $spell:43216 около вас",
    PillarArrow             = "Показывать стрелку, когда $spell:43216 около вас",
    RangeFrame              = "Показывать окно проверки дистанции на фазе дракондора (5 м)"
}

L:SetMiscLocalization{
    YellPull                = "У нас вечно хотят что-то отнять. Теперь мы вернем себе все. Любой, кто встанет у нас на пути, захлебнется в собственной крови! Империя Амани возрождается...ради мщения. И начнем мы...с вас!",
    YellKill                = "Я могу умереть... но империя Амани... никогда... не падет...",
    YellBear                = "Несколько новых трюков от брата медведя!",
    YellHawk                = "Вам никуда не спрятаться от орла!",
    YellLynx                = "Познакомьтесь с моими новыми братьями: клык и коготь!",
    YellDragon              = "Не нужно смотреть в небо, чтобы увидеть дракондора!",
    Bear                    = "медведя",
    Hawk                    = "орла",
    Lynx                    = "рыси",
    Dragon                  = "драконодора",
}
