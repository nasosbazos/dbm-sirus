local mod = DBM:NewMod("Halazzi", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4856 $"):sub(12, -3))

mod:SetCreatureID(23577)
mod:RegisterCombat("combat", 23577)

-- 23578 Boss
-- 24143 Lynx
-- 24224 Totem

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_DAMAGE",
    "SPELL_MISSED",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "UNIT_HEALTH"
)

local isDispeller = select(2, UnitClass("player")) == "PRIEST" or select(2, UnitClass("player")) == "PALADIN" 

local warnFrenzy            = mod:NewSpellAnnounce(43139, 1)
local warnTransfigureSoon   = mod:NewSoonAnnounce(43142, 1)
local warnTransfigure       = mod:NewSpellAnnounce(43142, 2)
local warnTotem             = mod:NewSpellAnnounce(43302, 3)
local warnFlameShock        = mod:NewTargetAnnounce(43303, 4)

local specWarnFrenzy        = mod:NewSpecialWarningDispel(43139, select(2, UnitClass("player")) == "HUNTER")
local specWarnDispelShock   = mod:NewSpecialWarningDispel(43303, isDispeller)

local timerFrenzyCD         = mod:NewCDTimer(20, 43139)
local timerTotemCD          = mod:NewCDTimer(16, 43302)
local timerFlameShockCD     = mod:NewCDTimer(10, 43303)
local timerFlameShock       = mod:NewTargetTimer(12, 43303)
local timerSaberLashCD      = mod:NewCDTimer(15, 43268)

local berserkTimer          = mod:NewBerserkTimer(600)

local warned = {true,true,true,true}

function mod:OnCombatStart(delay)
    timerFrenzyCD:Start(-delay)
    timerSaberLashCD:Start(10-delay)
    berserkTimer:Start(-delay)
    warned = {true,true,true,true}
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(43302) then
        warnTotem:Show()
        timerTotemCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(43303) then
        timerFlameShockCD:Start()
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(43268) and self:AntiSpam(1, "cleave") then
        timerSaberLashCD:Start()
    end
end
mod.SPELL_MISSED = mod.SPELL_DAMAGE

function mod:CancelTimers()
    timerFrenzyCD:Cancel()
    timerSaberLashCD:Cancel()
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(43142) then
        self:ScheduleMethod(2, "CancelTimers")
        warnTransfigure:Show()
        timerTotemCD:Start(6)
        timerFlameShockCD:Start(8)
    elseif args:IsSpellID(43139) then
        warnFrenzy:Show()
        specWarnFrenzy:Show()
        timerFrenzyCD:Start()
    elseif args:IsSpellID(43303) then
        warnFlameShock:Show(args.destName)
        specWarnDispelShock:Show(args.destName)
        timerFlameShock:Start(args.destName)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(43303) then
        timerFlameShock:Cancel(args.destName)
    end
end

function mod:Detransfigure()
    timerTotemCD:Cancel()
    timerFrenzyCD:Start()
    timerSaberLashCD:Start(10)
end

function mod:UNIT_HEALTH(uId)
    if self:GetUnitCreatureId(uId) == 23577 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.77 and warned[1] then
        warned[1] = false
        warned[4] = true
        warnTransfigureSoon:Show()
    elseif self:GetUnitCreatureId(uId) == 23577 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.52 and warned[2] then
        warned[2] = false
        warned[4] = true
        warnTransfigureSoon:Show()
    elseif self:GetUnitCreatureId(uId) == 23577 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.27 and warned[3] then
        warned[3] = false
        warned[4] = true
        warnTransfigureSoon:Show()
    elseif self:GetUnitCreatureId(uId) == 24143 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.25 and warned[4] then
        warned[4] = false
        self:ScheduleMethod(2, "Detransfigure")
    end
end