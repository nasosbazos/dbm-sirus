local mod = DBM:NewMod("Inciter", "DBM-Party-BC", 11)
local L = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 128 $"):sub(12, -3))

mod:SetCreatureID(18667)
mod:RegisterCombat("combat", 18667)

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS"
)

local warnChaosSoon        = mod:NewSoonAnnounce(33676, 4)

local timerChaos        = mod:NewBuffActiveTimer(15, 33676)
local timerChaosCD        = mod:NewCDTimer(40, 33676)
local timerStompCD        = mod:NewCDTimer(37, 33707)

mod:AddBoolOption("RemoveWeaponOnMindControl", true)

function mod:RemoveWeapon()
    if self:IsWeaponDependent() then
        PickupInventoryItem(16)
        PutItemInBackpack()
        PickupInventoryItem(17)
        PutItemInBackpack()
    elseif select(2, UnitClass("player")) == "HUNTER" then
        PickupInventoryItem(18)
        PutItemInBackpack()
    end
end

function mod:OnCombatStart(delay)
    timerChaosCD:Start(20)
    timerStompCD:Start(19)
    warnChaosSoon:Schedule(15)
    if self.Options.RemoveWeaponOnMindControl then
        mod:ScheduleMethod(19, "RemoveWeapon")
    end
end


function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(33676) then
        warnChaosSoon:Schedule(35)
        timerChaos:Start()
        timerChaosCD:Start()
        if args:IsPlayer() and self.Options.RemoveWeaponOnMindControl then
            mod:ScheduleMethod(39, "RemoveWeapon")
        end
    elseif args:IsSpellID(33707) then
        timerStompCD:Start()
    end
end