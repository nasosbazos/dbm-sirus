local mod = DBM:NewMod("Thorngrin", "DBM-Party-BC", 12)
local L = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 128 $"):sub(12, -3))

mod:SetCreatureID(17978)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_AURA_APPLIED"
)

local warnSacrifice       = mod:NewTargetAnnounce(34661)

local specWarnHellfire    = mod:NewSpecialWarningRun(39131,  mod:IsMelee())

local timerHellfireCD     = mod:NewCDTimer(18, 39131)
local timerSacrifice      = mod:NewTargetTimer(8, 34661)


function mod:OnCombatStart()
    timerHellfireCD:Start()
    specWarnHellfire:Schedule(16)
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(34661) then
        warnSacrifice:Show(args.destName)
        timerSacrifice:Start(args.destName)
    elseif args:IsSpellID(39131) then
        specWarnHellfire:Schedule(16)
        timerHellfireCD:Start()
    end
end