local mod = DBM:NewMod("Magtheridon", "DBM-Magtheridon");
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4820 $"):sub(12, -3))
mod:SetCreatureID(17257)

mod:RegisterCombat("yell", L.YellPull)
mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_PERIODIC_DAMAGE",
    "SPELL_INTERRUPT",
    "UNIT_SPELLCAST_SUCCEEDED",
    "CHAT_MSG_MONSTER_EMOTE",
    "CHAT_MSG_MONSTER_YELL"
)

local warnDebris                    = mod:NewSpellAnnounce(36449, 3)
local warnQuake                     = mod:NewAnnounce("WarnQuake", 3, "Interface\\Icons\\Spell_Misc_EmotionSad")
local warnNovaNormalSoon            = mod:NewSoonAnnounce(30616, 4)

local specWarnNovaNormal            = mod:NewSpecialWarningRun(30616)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnNovaSoon                  = mod:NewSoonAnnounce(305129, 2)
local warningNovaCast               = mod:NewCastAnnounce(305129, 4)
local warnHandOfMagt                = mod:NewTargetAnnounce(305131, 3)
local warnDevastatingStrike         = mod:NewTargetAnnounce(305134, 3, nil, mod:IsTank() or mod:IsHealer())
local warnPhase2soon                = mod:NewAnnounce("WarnPhase2soon", 1)

local specWarnNova                  = mod:NewSpecialWarningRun(305129)
local specWarnHandOfMagt            = mod:NewSpecialWarningYou(305131)
local specWarnHandStop              = mod:NewSpecialWarningCast(305131)
local specwarnDevastatingStrike     = mod:NewSpecialWarningTarget(305134, mod:IsTank())

local timerNovaNormalCD             = mod:NewCDTimer(60, 30616)
local timerQuakeCD                  = mod:NewTimer(50, "TimerQuakeCD", "Interface\\Icons\\Spell_Misc_EmotionSad")
local timerDebris                   = mod:NewCastTimer(16, 36449)
local timerPull                     = mod:NewTimer(120, "Pull", 305131)
local berserkTimer                  = mod:NewBerserkTimer(600)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerNovaCD                   = mod:NewCDTimer(80, 305129)
local timerHandOfMagt               = mod:NewTimer(10, "TimerHandOfMagt", 305131)
local timerHandOfMagtCD             = mod:NewCDTimer(20, 305131)
local timerDevastatingStrikeCD      = mod:NewCDTimer(15, 305134, nil, mod:IsTank() or mod:IsHealer())
local timerShatteredArmor           = mod:NewTargetTimer(30, 305135, nil, mod:IsTank() or mod:IsHealer())

mod:AddBoolOption("WarnPhase2soon",true)

local handTargets = {}
local lastQuake = 0
local fakeQuake = false

function mod:Nova()
    timerNovaNormalCD:Start()
    warnNovaNormalSoon:Schedule(55)
    self:UnscheduleMethod("Nova")
    self:ScheduleMethod(60, "Nova")
end

function mod:ExtendNova(extendBy)
    local elapsed, total = timerNovaNormalCD:GetTime()
    timerNovaNormalCD:Update(elapsed, total + extendBy)
    self:UnscheduleMethod("Nova")
    self:ScheduleMethod(total - elapsed + extendBy, "Nova")
    if total - elapsed > 5 then
        warnNovaNormalSoon:Cancel()
        warnNovaNormalSoon:Schedule(total - elapsed + extendBy - 5)
    end
end

function mod:Quake(timer)
    timerQuakeCD:Start(timer)
    self:UnscheduleMethod("Quake")
    self:ScheduleMethod(timer or 50, "Quake")
    if GetTime() - lastQuake > 10  then
        self:ExtendNova(7)
        lastQuake = GetTime()
    end
end

function mod:QuakeFakeDetection()
    if fakeQuake then
        fakeQuake = false
    else
        warnQuake:Show()
        self:Quake()
        self:PlaySound("mazafak")
    end
end

function mod:ExtendQuake(extendBy)
    local elapsed, total = timerQuakeCD:GetTime()
    timerQuakeCD:Update(elapsed, total + extendBy)
    self:UnscheduleMethod("Quake")
    self:ScheduleMethod(total - elapsed + extendBy, "Quake")
end

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal25") then
        fakeQuake = false
        self:Quake(30)
        self:Nova()
        berserkTimer:Start(-delay)
    elseif self:IsDifficulty("heroic25") then
        timerNovaCD:Start(-delay)
        timerHandOfMagtCD:Start(-delay)
        timerDevastatingStrikeCD:Start(30-delay)
        berserkTimer:Start(-delay)
        table.wipe(handTargets)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(305158, 305159, 305160) then
        warningNovaCast:Show()
        timerNovaCD:Start()
        specWarnNova:Show()
    elseif args:IsSpellID(305134) then
        local targetShattered = self:GetBossTarget(17257)
        warnDevastatingStrike:Show(targetShattered)
        specwarnDevastatingStrike:Show(targetShattered)
        timerDevastatingStrikeCD:Start()
        timerDevastatingStrikeCD:Schedule(15)
    elseif args:IsSpellID(30616) then
        specWarnNovaNormal:Show()
        self:Nova()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(30572) then
        fakeQuake = true
    end
end

function mod:WarnHand()
    warnHandOfMagt:Show(table.concat(handTargets, "<, >"))
    table.wipe(handTargets)
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(305131) then
        if args:IsPlayer() then
            specWarnHandOfMagt:Show()
            specWarnHandStop:Schedule(8)
            timerHandOfMagt:Start()
            self:PlaySound("t_zaminirovali")
        end
        handTargets[#handTargets + 1] = args.destName
        self:UnscheduleMethod("WarnHand")
        self:ScheduleMethod(0.1, "WarnHand")
        timerHandOfMagtCD:Start()
    elseif args:IsSpellID(305135) then
        timerShatteredArmor:Start(args.destName)
        if args:IsPlayer() then
            self:PlaySound("ebany_rot", "vyebav", "vnoge")
        end
    end
end

function mod:SPELL_PERIODIC_DAMAGE(args)
    if args:IsSpellID(305312) and self:AntiSpam(30, "fail") then
        self:PlaySound("t_pohuy", "netmozga")
    elseif args:IsSpellID(34435) and args:IsPlayer() and self:AntiSpam(30, "rain") then
        self:PlaySound("rain")
    end
end

function mod:SPELL_INTERRUPT(args)
    if args:IsSpellID(305132) and args:IsPlayer() then
        SendChatMessage(L.HandFail:format(args.destName, GetSpellLink(args.extraSpellId)), "RAID")
        self:PlaySound("s_durak_takoi", "fiasco")
    end
end

function mod:UNIT_SPELLCAST_SUCCEEDED(unit, spellName, ...)
    if UnitName(unit) == L.name and spellName == L.Quake then
        self:ScheduleMethod(0.1, "QuakeFakeDetection")
    end
end

function mod:CHAT_MSG_MONSTER_EMOTE(msg)
    if msg == L.YellPullAcolytes then
        timerPull:Start()
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellPhase2 then
        if self:IsDifficulty("normal25") then
            warnDebris:Show()
            timerDebris:Start()
            self:ExtendNova(13)
            self:ExtendQuake(13)
        elseif self:IsDifficulty("heroic25") then
            timerNovaCD:Cancel()
            warnPhase2soon:Show()
        end
    end
end

function mod:OnCombatEnd()
    self:UnscheduleMethod("Quake")
    self:UnscheduleMethod("Nova")
    table.wipe(handTargets)
end
