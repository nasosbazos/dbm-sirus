﻿if GetLocale() ~= "ruRU" then return end

local L

--Attumen
L = DBM:GetModLocalization("Attumen")

L:SetGeneralLocalization{
    name = "Аттумен Охотник"
}

L:SetWarningLocalization{
    WarnPhase2Soon        = "Скоро фаза 2",
    WarnAttumen           = "Призыв Аттумена"
}

L:SetOptionLocalization{
    WarnPhase2Soon        = "Предупреждать о переходе на пешую фазу",
    WarnAttumen           = "Обьявлять от появлении Аттумена"
}

L:SetMiscLocalization{
    YellPhase2            = "Ко мне, Полночь! Разгоним этот жалкий сброд!",
    EmoteAttumen          = "%s зовет своего господина!",
    KillAttumen           = "Всегда знал... когда-нибудь... я стану... добычей."
}


--Moroes
L = DBM:GetModLocalization("Moroes")

L:SetGeneralLocalization{
    name = "Мороуз"
}

L:SetWarningLocalization{
    DBM_MOROES_VANISH_FADED    = "Исчезновение рассеивается",
    WarnPhase2Soon             = "Скоро фаза 2",
}

L:SetTimerLocalization{
    Phase2                     = "Фаза 2",
    TimerEmerge                = "Появление"
}

L:SetOptionLocalization{
    DBM_MOROES_VANISH_FADED    = "Show vanish fade warning",
    WarnPhase2Soon             = "Предупреждать о приближении второй фазы",
    Phase2                     = "Отсчитывать время до второй фазы.",
    TimerEmerge                = "Отсчитывать время до рассеивания $spell:29448"
}

L:SetMiscLocalization{
    YellPull                   = "Хм, нежданные гости. Надо бы приготовиться..."
}


-- Maiden of Virtue
L = DBM:GetModLocalization("Maiden")

L:SetGeneralLocalization{
    name = "Благочестивая дева"
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
    RangeFrame            = "Отображать окно проверки дистанции (12)",
    HealthFrame           = "Отображать здоровье босса и прочность щита"
}

L:SetMiscLocalization{
    YellPull              = "Я не потерплю такого поведения.",
    YellRepentence1       = "Отбросьте ваши грязные мысли.",
    YellRepentence2       = "Я выжгу вашу нечестивость!"
}

-- Romulo and Julianne
L = DBM:GetModLocalization("RomuloAndJulianne")

L:SetGeneralLocalization{
    name = "Ромуло и Джулианна"
}

L:SetWarningLocalization{
    warningPosion       = "%s на |3-5(>%s<) (%s)"
}

L:SetTimerLocalization{
    TimerCombatStart    = "Начало боя"
}

L:SetOptionLocalization{
    TimerCombatStart    = "Отсчет времени до начала боя"
}

L:SetMiscLocalization{
    Event               = "Сегодня... мы увидим историю любви!",
    RJ_Pull             = "Что ты за дьявол, что меня так мучишь?",
    DBM_RJ_PHASE2_YELL  = "Приди же, ласковая ночь, верни мне моего Ромуло!",
    Romulo              = "Ромуло",
    Julianne            = "Джулианна"
}


-- Big Bad Wolf
L = DBM:GetModLocalization("BigBadWolf")

L:SetGeneralLocalization{
    name = "Злой и страшный серый волк"
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
    RRHIcon     = DBM_CORE_AUTO_ICONS_OPTION_TEXT:format(30753)
}

L:SetMiscLocalization{
    YellPull    = "Кем бы мне тут закусить?"
}


-- Wizard of Oz
L = DBM:GetModLocalization("Oz")

L:SetGeneralLocalization{
    name = "Страна Оз"
}

L:SetWarningLocalization{
}

L:SetTimerLocalization{
    TimerCombatStart          = "Начало боя"
}

L:SetOptionLocalization{
    TimerCombatStart          = "Отсчет времени до начала боя",
    AnnounceBosses            = "Объявлять об активации боссов",
    ShowBossTimers            = "Отображать таймеры до активации боссов",
    OptionRangeFrameP2        = "Отображать окно проверки дистанции (10м) в фазе 2"
}

L:SetMiscLocalization{
    YellOzPrePull             = "Сегодня мы измерим глубины человеческой души вместе с этой потерявшейся маленькой девочкой, которой верные спутники помогают искать путь домой!",
    YellDorothee              = "Тито, мы просто обязаны найти дорогу домой! Старый волшебник – наша единственная надежда. Пугало, Рычун, Нержавей, вы... ой, к нам кто-то пришел!",
    YellRoar                  = "Я вас не боюсь! Совсем! Хотите сражаться? Хотите, да? Ну же! Я буду драться, даже если мне свяжут лапы за спиной!",
    YellStrawman              = "И что же мне с вами делать? Никак не соображу.",
    YellTinhead               = "Мне очень нужно сердце. Может, забрать твое?",
    YellCrone                 = "Скоро все закончится!",
    WarnRoar                  = "Хохотун",
    WarnStrawman              = "Балбес",
    WarnTinhead               = "Медноголовый",
    WarnTito                  = "Тито",
    WarnCrone                 = "Ведьма"
}


-- Zluker
L = DBM:GetModLocalization("Zluker")

L:SetGeneralLocalization{
    name = "Злюкер"
}

L:SetWarningLocalization{
}

L:SetTimerLocalization{
    TimerCombatStart    = "Битва начнется через"
}

L:SetOptionLocalization{
    TimerCombatStart    = "Отсчет времени до начала битвы"
}

L:SetMiscLocalization{
    YellPull            = "Дамы и господа, добро пожаловать... ОХ!",
    YellZluker          = "Вместе мы неудержимы!"
}


-- Curator
L = DBM:GetModLocalization("Curator")

L:SetGeneralLocalization{
    name = "Смотритель"
}

L:SetWarningLocalization{
    WarnUnstableTar           = "Нестабильная энергия на >%s<",
    WarnDensePoison           = "%s на |3-5(>%s<) (%s)"
}

L:SetTimerLocalization{
    TimerRunesBam             = "Взрыв!"
}

L:SetOptionLocalization{
    WarnUnstableTar           = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.spell:format(305309, GetSpellInfo(305309) or "unknown"),
    TimerRunesBam             = "Отсчет времени до взрыва $spell:305296",
    RangeFrame                = "Окно проверки дистанции когда на вас $spell:305305",
    SwapShaman                = "Перемещать шамана между группами 1 и 2 для снятия $spell:305303",
    WarnDensePoison           = "Объявлять количестве стаков на цели $spell:305299"
}

L:SetMiscLocalization{
    YellPull                  = "Вход только по приглашениям.",
    YellOOM                   = "Ваш запрос не может быть обработан.",
    YellP2                    = "В случае неподчинения будут приняты меры.",
    YellKill                  = "Смотритель завершает ра-бо-ту..."
}


-- Terestian Illhoof
L = DBM:GetModLocalization("TerestianIllhoof")

L:SetGeneralLocalization{
    name = "Терестиан Больное Копыто"
}

L:SetWarningLocalization{
    WarnDart    = "%s на |3-5(>%s<) (%s)"
}

L:SetTimerLocalization{
}

L:SetOptionLocalization{
    RangeFrame          = "Окно проверки дистанции когда на вас $spell:305351",
    ShowMinSeedHP       = "Отображать минимальную прочность $spell:305360 среди активных целей",
    ShowEverySeedHP     = "Отображать прочность каждой цели $spell:305360",
    WarnDart            = "Объявлять количестве стаков на цели $spell:305367",
    AlwaysShowMediation = "Отображать время до получения $spell:305354 для всех целей"
}

L:SetMiscLocalization{
    YellPull    = "О, вы как раз вовремя. Ритуал вот-вот начнется!",
    Kilrek      = "Кил'рек",
    DChains     = "Демонические цепи",
    Seed        = "Семя порчи",
    YellKill    = "Умираю за тебя, о великий..."
}


-- Shade of Aran
L = DBM:GetModLocalization("Aran")

L:SetGeneralLocalization{
    name = "Тень Арана"
}

L:SetWarningLocalization{
    DBM_ARAN_DO_NOT_MOVE    = "Не двигайтесь!",
    SpecWarnFreeze          = "Вы будете заморожены!"
}

L:SetTimerLocalization{
    TimerSpecial            = "Следующая спец-способность",
    TimerSpecialH           = "Следующая спец-способность"
}

L:SetOptionLocalization{
    DBM_ARAN_DO_NOT_MOVE    = "Спец-предупреждение для $spell:30004",
    TimerSpecial            = "Отсчет времени до следующей спец-способности",
    TimerSpecialH           = "Отсчет времени до следующей спец-способности(героич.)",
    SpecWarnFreeze          = "Предупреждение о том что вы будете заморожены"
}

L:SetMiscLocalization{
    Familliar               = "Фамильяр",
    TimerBossRespawn        = "Респавн"
}


--Netherspite
L = DBM:GetModLocalization("Netherspite")

L:SetGeneralLocalization{
    name = "Пустогнев"
}

L:SetWarningLocalization{
    DBM_NS_WARN_PORTAL_SOON    = "Фаза порталов через 5 секунд",
    DBM_NS_WARN_BANISH_SOON    = "Фаза изгнания через 5 секунд",
    warningPortal              = "Фаза порталов",
    warningBanish              = "Фаза изгнания",
    SpecWarnKickNow            = "Прерывание"
}

L:SetTimerLocalization{
    timerPortalPhase           = "Фаза порталов",
    timerBanishPhase           = "Фаза изгнания",
    TimerPortals               = "Появление порталов",
    TimerNormalPhase           = "Обычная фаза"
}

L:SetOptionLocalization{
    DBM_NS_WARN_PORTAL_SOON    = "Предупреждать заранее о фазе порталов",
    DBM_NS_WARN_BANISH_SOON    = "Предупреждать заранее о фазе изгнания",
    warningPortal              = "Предупреждать о фазе порталов",
    warningBanish              = "Предупреждать о фазе изгнания",
    timerPortalPhase           = "Отсчет времени до окончания фазы порталов",
    timerBanishPhase           = "Отсчет времени до окончания фазы изгнания",
    SpecWarnKickNow            = "Спец-предупреждение, когда вы должы прервать заклинание",
    TimerPortals               = "Появление порталов",
    TimerNormalPhase           = "Обычная фаза"
}

L:SetMiscLocalization{
    DBM_NS_EMOTE_PHASE_2       = "%s впадает в предельную ярость!",
    DBM_NS_EMOTE_PHASE_1       = "%s издает крик, отступая, открывая путь Пустоте."
}


--Prince Malchezaar
L = DBM:GetModLocalization("Prince")

L:SetGeneralLocalization{
    name = "Принц Малчезар"
}

L:SetWarningLocalization{
    WarnNextPhase               = "Фаза %s",
    WarnArcaneCleave            = "%s на |3-5(>%s<) (%s)",
}

L:SetTimerLocalization{
}

L:SetOptionLocalization{
    WarnNextPhase               = "Предупреждать о переходе на следующую фазу",
    RemoveHysteriaOnEnfeeble    = "Отменять действие $spell:49016 при получении $spell:30843",
    RangeFrame                  = "Окно проверки дистанции (10м)",
    RemoveWeaponOnMindControl   = "Снимать оружие при подчинении $spell:305447",
    RemoveDefBuffsOnMindControl = "Удалять защитные бафы при подчинении $spell:305447",
    WarnArcaneCleave            = "Объявлять количестве стаков на цели $spell:305427"
}

L:SetMiscLocalization{
    YellPull                    = "Безумие привело вас сюда, я стану вашей погибелью.",
    YellPhase2                  = "Глупцы! Время – это огонь, сжигающий вас!",
    YellPhase3                  = "Как вы осмелились бросить вызов столь колоссальной мощи?",
    YellInfernal1               = "Мне открыты все реальности, все измерения!",
    YellInfernal2               = "Вы противостоите не только Мальчезаару, но и всем подвластным мне легионам!",
    FlameWorld                  = "Огненные просторы",
    IceWorld                    = "Ледяная пустошь",
    BlackForest                 = "Черный лес",
    LastPhase                   = "Финал"
}


-- Nightbane
L = DBM:GetModLocalization("Nightbane")

L:SetGeneralLocalization{
    name = "Ночная Погибель"
}

L:SetWarningLocalization{
    WarnAirPhase                = "Воздушная фаза"
}

L:SetTimerLocalization{
    timerNightbane              = "Приземление",
    timerAirPhase               = "Воздушная фаза"
}

L:SetOptionLocalization{
    SetIconOnPyromancer         = "Устанавливать иконки на Пиромантов",
    RemoveWeaponOnMindControl   = "Снимать оружие при подчинении",
    RemoveDefBuffsOnMindControl = "Удалять защитные бафы при подчинении $spell:305386",
    WarnAirPhase                = "Предупреждать о начале воздушной фазы",
    timerNightbane              = "Отсчет времени до прибытия босса",
    timerAirPhase               = "Отсчет времени до окончания воздушной фазы"
}

L:SetMiscLocalization{
    EmoteStart                  = "Древнее существо пробуждается вдалеке...",
    YellPull                    = "Ну и глупцы! Я быстро покончу с вашими страданиями!",
    YellAir                     = "Жалкий гнус! Я изгоню тебя из воздуха!"
}


-- Named Beasts
L = DBM:GetModLocalization("Shadikith")

L:SetGeneralLocalization{
    name = "Shadikith the Glider"
}

L = DBM:GetModLocalization("Hyakiss")

L:SetGeneralLocalization{
    name = "Hyakiss the Lurker"
}

L = DBM:GetModLocalization("Rokad")

L:SetGeneralLocalization{
    name = "Rokad the Ravager"
}
