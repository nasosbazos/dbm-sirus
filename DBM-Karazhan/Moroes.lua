local mod = DBM:NewMod("Moroes", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4820 $"):sub(12, -3))

mod:SetCreatureID(15687, 17007, 19872, 19873, 19874, 19875, 19876)

-- 17007 - Lady Keira Berrybuck (holy paladin)
-- 19872 - Lady Catriona Von'Indi (holy priest)
-- 19873 - Lord Crispin Ference (prot warrior)
-- 19874 - Baron Rafe Dreuger (ret paladin)
-- 19875 - Baroness Dorothea Millstipe (shadow priest)
-- 19876 - Lord Robin Daris (arms warrior)

mod:RegisterCombat("combat", 15687)
--mod:RegisterCombat("yell", L.YellPull)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_CAST_SUCCESS",
    "SPELL_MISSED",
    "UNIT_DIED"
)

local warnVanishSoon        = mod:NewSoonAnnounce(29448, 2)
local warnVanish            = mod:NewSpellAnnounce(29448, 3)
local warnGarrote           = mod:NewTargetAnnounce(37066, 4)
local warnGougeSoon         = mod:NewSoonAnnounce(29425, 4)
local warnGouge             = mod:NewTargetAnnounce(29425, 4)
local warnBlind             = mod:NewTargetAnnounce(34694, 3)
local warnMortalStrike      = mod:NewTargetAnnounce(29572, 2)
local warnHammer            = mod:NewTargetAnnounce(13005, 4)
local warnManaBurn          = mod:NewCastAnnounce(29405, 3)
local warnGreaterHeal       = mod:NewCastAnnounce(35096, 3)
local warnHolyLight         = mod:NewCastAnnounce(29562, 3)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnPhase2Soon        = mod:NewAnnounce("WarnPhase2Soon", 2, 40810)
local warnDeathMark         = mod:NewTargetAnnounce(305470, 4)
local warnKillingSpree      = mod:NewSpellAnnounce(305460, 4)
local warnKillingSpreeSoon  = mod:NewSoonAnnounce(305460, 3)

local specWarnDance         = mod:NewSpecialWarningRun(305472, not mod:IsTank() or not mod:IsHealer())
local specWarnDeathMark     = mod:NewSpecialWarningYou(305470)

local timerVanishCD         = mod:NewCDTimer(26, 29448)
local timerEmerge           = mod:NewTimer(10, "TimerEmerge", 29448)
local timerGouge            = mod:NewTargetTimer(6, 29425)
local timerGougeCD          = mod:NewCDTimer(46, 29425)
local timerBlind            = mod:NewTargetTimer(10, 34694)
local timerMortalStrike     = mod:NewTargetTimer(5, 29572)
local timerHammerCD         = mod:NewCDTimer(12, 13005)
local timerPainCD           = mod:NewCDTimer(7, 34441)
local timerDivineShieldCD   = mod:NewCDTimer(30, 41367)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerDanceCD          = mod:NewCDTimer(20, 305472)
local timerDance            = mod:NewTargetTimer(10, 305478)
local timerPhase2           = mod:NewTimer(180, "Phase2", 40810)
local timerPierceCD         = mod:NewCDTimer(10, 305464)
local timerWoundCD          = mod:NewCDTimer(10, 305463)
local timerDeathMark        = mod:NewTargetTimer(10, 305470)
local timerDeathMarkCD      = mod:NewCDTimer(25, 305470)
local timerKillingSpree     = mod:NewCastTimer(5, 305460)
local timerKillingSpreeCD   = mod:NewCDTimer(85, 305460)

local berserkTimer          = mod:NewBerserkTimer(480)

local phase2 = false

function mod:Gouge()
    if self:IsInCombat() then 
        timerGougeCD:Start()
        warnGougeSoon:Cancel()
        warnGougeSoon:Schedule(41)
        self:UnscheduleMethod("Gouge")
        self:ScheduleMethod(46, "Gouge")
    end
end

function mod:Hammer()
    if self:IsInCombat() then 
        timerHammerCD:Start()
        self:UnscheduleMethod("Hammer")
        self:ScheduleMethod(12, "Hammer")
    end
end

function mod:Pain()
    if self:IsInCombat() then 
        timerPainCD:Start()
        self:UnscheduleMethod("Pain")
        self:ScheduleMethod(7, "Pain")
    end
end

function mod:Phase2()
    phase2 = true
    timerWoundCD:Start(5)
    timerPierceCD:Start()
    timerDeathMarkCD:Start()
    timerKillingSpreeCD:Start()
    warnKillingSpreeSoon:Schedule(80)
end

function mod:OnCombatStart(delay)
    if self:GetBossTarget(17007) then
        timerDivineShieldCD:Start(10-delay)
    end
    if self:GetBossTarget(19874) then
        timerHammerCD:Start(-delay)
        self:ScheduleMethod(12, "Hammer")
    end
    if self:GetBossTarget(19875) then
        timerPainCD:Start(3-delay)
        self:ScheduleMethod(3, "Pain")
    end
    if self:IsDifficulty("normal10") then
        timerVanishCD:Start(31-delay)
        warnVanishSoon:Schedule(26-delay)
        timerGougeCD:Start(23-delay)
        warnGougeSoon:Schedule(18-delay)
    elseif self:IsDifficulty("heroic10") then
        phase2 = false
        warnPhase2Soon:Schedule(175)
        timerDanceCD:Start()
        timerPhase2:Start()
        berserkTimer:Start()
        self:ScheduleMethod(180, "Phase2")
    end
    self:PlaySound("ya_vas_ne_zval")
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(29405) then
        warnManaBurn:Show()
    elseif args:IsSpellID(35096) then
        warnGreaterHeal:Show()
    elseif args:IsSpellID(29562) then
        warnHolyLight:Show()
    elseif args:IsSpellID(305464) and phase2 then
        self:PlaySound("taa")
        timerPierceCD:Start()
    elseif args:IsSpellID(305463) and phase2 then
        self:PlaySound("sha")
        timerWoundCD:Start()
    elseif args:IsSpellID(305472) then
        timerDanceCD:Start()
        specWarnDance:Show()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(29448) then
        timerEmerge:Start()
        timerVanishCD:Schedule(10)
        warnVanish:Show()
        warnVanishSoon:Schedule(31)
    elseif args:IsSpellID(41367) then
        timerDivineShieldCD:Start()
    elseif args:IsSpellID(29425) then
        self:Gouge()
    elseif args:IsSpellID(13005) then
        self:Hammer()
    elseif args:IsSpellID(34441) then
        self:Pain()
    elseif args:IsSpellID(305460) then
        warnKillingSpree:Show()
        timerKillingSpreeCD:Start()
        warnKillingSpreeSoon:Schedule(80)
        self:PlaySound("ora", "muda", "atata")
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(29425) then
        self:PlaySound("cheeki-breeki")
        warnGouge:Show(args.destName)
        timerGouge:Show(args.destName)
        self:Gouge()
    elseif args:IsSpellID(13005) then
        warnHammer:Show(args.destName)
    elseif args:IsSpellID(34694) then
        warnBlind:Show(args.destName)
        timerBlind:Show(args.destName)
    elseif args:IsSpellID(29572) then
        warnMortalStrike:Show(args.destName)
        timerMortalStrike:Show(args.destName)
    elseif args:IsSpellID(37066) then
        warnGarrote:Show(args.destName)
    elseif args:IsSpellID(37023) then
        timerVanishCD:Cancel()
        warnVanishSoon:Cancel()
    elseif args:IsSpellID(305470) then
        if args:IsPlayer() then 
            self:PlaySound("omaeva")
            specWarnDeathMark:Show()
        end
        warnDeathMark:Show(args.destName)
        timerDeathMark:Start(args.destName)
        timerDeathMarkCD:Start()
    elseif args:IsSpellID(305478) then
        timerDance:Start(args.destName)
        if args:IsPlayer() then 
            self:PlaySound("djeban","gandalf","cigan","hardbass","upkicks", "scizo", "kakzhit")
        end
    elseif args:IsSpellID(305479) and args:IsPlayer() then
        self:ScheduleMethod(5, "PlaySound", "pokushat")
    elseif args:IsSpellID(305486) and args:IsPlayer() then
        self:PlaySound("tortik")
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(34694) then
        timerBlind:Cancel(args.destName)
    end
end

function mod:SPELL_MISSED(args)
    if args:IsSpellID(29425) then
        self:Gouge()
    end
end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if cid == 17007 then
        timerDivineShieldCD:Cancel()
    elseif cid == 19874 then
        timerHammerCD:Cancel()
        self:UnscheduleMethod("Hammer")
    elseif cid == 19875 then
        timerPainCD:Cancel()
        self:UnscheduleMethod("Pain")
    end
end

function mod:OnCombatEnd()
    self:UnscheduleMethod("Gouge")
    self:UnscheduleMethod("Hammer")
    self:UnscheduleMethod("Pain")
end