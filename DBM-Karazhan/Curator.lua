local mod = DBM:NewMod("Curator", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4841 $"):sub(12, -3))
mod:SetCreatureID(15691,34436,34437,34438)

-- 34436(boss1) - Смотритель AR-NE-035
-- 34437(boss2) - Смотритель PO-N-21
-- 34438(boss3) - Смотритель LI-G-163

mod:RegisterCombat("combat")
--mod:RegisterCombat("yell", L.YellPull)
--mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REMOVED",
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "CHAT_MSG_MONSTER_YELL",
    "UNIT_DIED",
    "SPELL_DAMAGE",
    "SPELL_DISPEL"
)

local warnEvoSoon               = mod:NewPreWarnAnnounce(30254, 10, 2)
local warnEvo                   = mod:NewSpellAnnounce(30254, 3)
local warnArcaneInfusion        = mod:NewSpellAnnounce(30403, 3)
local warnPhase2                = mod:NewPhaseAnnounce(2)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnUnstableTar           = mod:NewAnnounce("WarnUnstableTar", 1, 305309)
local warnCond                  = mod:NewTargetAnnounce(305305, 4)
local warnDensePoison           = mod:NewAnnounce("WarnDensePoison", 2, 305299, mod:IsTank() or mod:IsHealer())
local warnAnnihilation          = mod:NewSpellAnnounce(305312, 3)

local specWarnRunes             = mod:NewSpecialWarningRun(305296)
local specWarnPoisonNova        = mod:NewSpecialWarningStack(305302, not mod:IsTank(), 2)
local specWarnAnnihilationKick  = mod:NewSpecialWarningInterupt(305312)
local specWarnCond              = mod:NewSpecialWarningYou(305305)
local specWarnCondNear          = mod:NewSpecialWarningClose(305305)

local yellCond                  = mod:NewYell(305305)

local timerEvo                  = mod:NewBuffActiveTimer(20, 30254)
local timerNextEvo              = mod:NewNextTimer(115, 30254)
local berserkTimer              = mod:NewBerserkTimer(600)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerRunesCD              = mod:NewCDTimer(25, 305296)
local timerRunesBam             = mod:NewTimer(8, "TimerRunesBam", 305314)
local timerPoisonCloudCD        = mod:NewCDTimer(10, 305300)
local timerPoisonBoltCD         = mod:NewCDTimer(13, 305303)
local timerDensePoisonCD        = mod:NewCDTimer(11, 305299, nil, mod:IsTank())
local timerAnnihilationCD       = mod:NewCDTimer(23, 305312)
local timerCondCD               = mod:NewCDTimer(11, 305305)

mod:AddSetIconOption("SetIconOnCond", 305305 , {8}, true)
mod:AddBoolOption("RangeFrame", true)
mod:AddBoolOption("SwapShaman", false)

local unstableTargets = {}

local function checkEntry(t, val)
    for i, v in ipairs(t) do
        if v == val then
            return true
        end
    end
    return false
end

local function switchCurator()
    if not UnitAura("boss1", GetSpellInfo(305313), nil, "HARMFUL") then
        timerRunesCD:Start()
    elseif not UnitAura("boss2", GetSpellInfo(305313), nil, "HARMFUL") then
        timerPoisonCloudCD:Start()
        timerPoisonBoltCD:Start()
    elseif not UnitAura("boss3", GetSpellInfo(305313), nil, "HARMFUL") then
        timerAnnihilationCD:Start()
        timerCondCD:Start()
    end
end

local function searchUnstable()
    table.wipe(unstableTargets)
    for i=1,10 do
        if UnitAura("raid".. i, GetSpellInfo(305309)) then
            unstableTargets[#unstableTargets + 1] = UnitName("raid".. i)
        end
    end
    warnUnstableTar:Show(table.concat(unstableTargets, "<, >"))
end

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal10") then
        self.combatInfo.killMobs = {[15691] = true}
        berserkTimer:Start(-delay)
        timerNextEvo:Start(107-delay)
        warnEvoSoon:Schedule(99-delay)    
    elseif self:IsDifficulty("heroic10") then
        self.combatInfo.killMobs[15691] = false
        table.wipe(unstableTargets)
        switchCurator()
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellOOM then
        warnEvoSoon:Cancel()
        warnEvo:Show()
        timerNextEvo:Start()
        timerEvo:Start()
        warnEvoSoon:Schedule(95)
    elseif msg == L.YellP2 then
        warnPhase2:Show()
        warnEvoSoon:Cancel()
        timerNextEvo:Cancel()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(30403) then
        warnArcaneInfusion:Show()
    elseif args:IsSpellID(305298) and self:AntiSpam(20, "boom") then
        self:UnscheduleMethod("PlaySound", "bomb_d")
        self:PlaySound("terror_wins")
    elseif args:IsSpellID(305313) and self:AntiSpam(1, "inactive") and self:IsInCombat() then
        self:PlaySound("strashno")
        switchCurator()
    elseif args:IsSpellID(305305) then
        timerCondCD:Start()
        warnCond:Show(args.destName)
        if self.Options.SetIconOnCond then
            self:SetIcon(args.destName, 8)
        end
        if args:IsPlayer() then
            self:PlaySound("t_zaminirovali")
            specWarnCond:Show()
            yellCond:Yell()
            if self.Options.RangeFrame then
                DBM.RangeCheck:Show(10)
            end
        else
            local uId = DBM:GetRaidUnitId(args.destName)
            if uId and CheckInteractDistance(uId, 3) then
                specWarnCondNear:Show(args.destName)
                self:PlaySound("othodi", "podalshe")
            end
        end
    elseif args:IsSpellID(305309) then
        searchUnstable()
        if args:IsPlayer() then
            self:PlaySound("s_najmi2")
        end
    elseif args:IsSpellID(305299) then
        warnDensePoison:Show(args.spellName, args.destName, 1)
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(305302) then
        if args:IsPlayer() and args.amount >= 2 then
            specWarnPoisonNova:Show(args.amount)
        end
    elseif args:IsSpellID(305299) then
        warnDensePoison:Show(args.spellName, args.destName, args.amount)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(305309) then
        searchUnstable()
    elseif args:IsSpellID(305305) then
        if self.Options.SetIconOnCond then
            self:SetIcon(args.destName, 0)
            if self.Options.RangeFrame then
                DBM.RangeCheck:Hide()
            end
        end
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(305296) then    
        self:PlaySound("bomb_p")
        self:ScheduleMethod(10.1, "PlaySound", "bomb_d")
        specWarnRunes:Show()
        timerRunesCD:Start()
        timerRunesBam:Start()
    elseif args:IsSpellID(305300) then
        timerPoisonCloudCD:Start()
    elseif args:IsSpellID(305303) then
        timerPoisonBoltCD:Start()
    elseif args:IsSpellID(305312) then
        timerAnnihilationCD:Start()
        if checkEntry(unstableTargets, UnitName("player")) then
            self:PlaySound("s_najmi", "t_huyar")
            specWarnAnnihilationKick:Show()
        else
            self:PlaySound("pushka")
            warnAnnihilation:Show()
        end
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(305299) then
        timerDensePoisonCD:Start()
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(305312) and self:AntiSpam(10, "pohuy") then
        self:PlaySound("t_pohuy")
    end
end

function mod:SPELL_DISPEL(args)
    if args:IsSpellID(52025) and self.Options.SwapShaman and DBM:GetRaidRank() == 2 and type(args.extraSpellId) == "number" and args.extraSpellId == 305303 and DBM:GetRaidClass(args.destName) == "SHAMAN" then
        local shamanGroup = DBM:GetRaidSubgroup(args.destName)
        local shamanId = DBM:GetRaidUnitId(args.destName)
        local targetGroup, targetId
        for i=1,GetNumRaidMembers() do
            local g = select(3, GetRaidRosterInfo(i))
            if shamanGroup ~= g then
                targetGroup = g
                targetId = i
                if not UnitAura("raid"..i, GetSpellInfo(305303), nil, "HARMFUL") then
                    break
                end
            end
        end
        if targetGroup and targetId then
            SetRaidSubgroup(targetId, 3)
            SetRaidSubgroup(shamanId, targetGroup)
            SetRaidSubgroup(targetId, (targetGroup==1) and 2 or 1)
        end
    end
end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if cid == 34436 then
        self:PlaySound("noice")
    end
end

function mod:OnCombatEnd()
    self:UnscheduleMethod("PlaySound", "bomb_d")
end
