local mod = DBM:NewMod("BigBadWolf", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4500 $"):sub(12, -3))
mod:SetCreatureID(17521)
mod:RegisterCombat("yell", L.YellPull)
mod:SetUsedIcons(8)

mod:RegisterEvents(
    "SPELL_AURA_APPLIED",
    "SPELL_CAST_SUCCESS"
)

local warningFearSoon   = mod:NewSoonAnnounce(30752, 2)
local warningFear       = mod:NewSpellAnnounce(30752, 3)
local warningRRHSoon    = mod:NewSoonAnnounce(30753, 3)
local warningRRH        = mod:NewTargetAnnounce(30753, 4)

local specWarnRRH       = mod:NewSpecialWarningYou(30753)

local timerRRH          = mod:NewTargetTimer(20, 30753)
local timerRRHCD        = mod:NewNextTimer(30, 30753)
local timerFearCD       = mod:NewNextTimer(24, 30752)

mod:AddSetIconOption("RRHIcon", 30753, {8}, true)

function mod:OnCombatStart(delay)
    if mod:IsDifficulty("normal10") then
        timerRRHCD:Start(-delay)
        timerFearCD:Start(-delay)
        warningRRHSoon:Schedule(25-delay)
        warningFearSoon:Schedule(24-delay)
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(30753) then
        warningRRH:Show(args.destName)
        timerRRH:Start(args.destName)
        timerRRHCD:Start()
        warningRRHSoon:Cancel()
        warningRRHSoon:Schedule(25)
        if args:IsPlayer() then
            specWarnRRH:Show()
            self:PlaySound("begi")
        end
        if self.Options.RRHIcon then
            self:SetIcon(targetname, 8, 20)
        end
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(30752) then
        warningFear:Show()
        warningFearSoon:Cancel()
        warningFearSoon:Schedule(19)
        timerFearCD:Start()
    end
end
