local mod = DBM:NewMod("FlameLeviathan", "DBM-Ulduar")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4451 $"):sub(12, -3))

mod:SetCreatureID(33113)

mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell",L.YellKill)

mod:RegisterEvents(
    "SPELL_AURA_REMOVED",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REFRESH",
    "SPELL_CAST_SUCCESS",
    "SPELL_SUMMON"
)

local warnHodirsFury        = mod:NewTargetAnnounce(62297)
local pursueTargetWarn      = mod:NewAnnounce("PursueWarn", 2, 62374)
local warnNextPursueSoon    = mod:NewAnnounce("warnNextPursueSoon", 3, 62374)
local warnStacksLost        = mod:NewAnnounce("WarnStacksLost", 3 , 68605)

local warnSystemOverload    = mod:NewSpecialWarningSpell(62475)
local specWarnPursueYou     = mod:NewSpecialWarningYou(62374)
local warnWardofLife        = mod:NewSpecialWarning("warnWardofLife")
local warnStacksSoon        = mod:NewSpecialWarning("WarnStacksSoon")

local timerSystemOverload   = mod:NewBuffActiveTimer(20, 62475)
local timerPursued          = mod:NewTargetTimer(35, 62374)
local timerFlameVentsCD     = mod:NewCDTimer(20, 62396)

local guids = {}
local timers = {}
local stacks = {}

function mod:flameVentsTimer()
    timerFlameVentsCD:Start()
    self:UnscheduleMethod("flameVentsTimer")
    self:ScheduleMethod(20, "flameVentsTimer")
end

local function buildGuidTable()
    table.wipe(guids)
    table.wipe(timers)
    table.wipe(stacks)
    for i = 1, GetNumRaidMembers() do
        guids[UnitGUID("raid"..i.."pet") or ""] = UnitName("raid"..i)
    end
end

function mod:OnCombatStart(delay)
    buildGuidTable()
    self:PlaySound("basscannon")
    timerFlameVentsCD:Start(-delay)
    self:UnscheduleMethod("flameVentsTimer")
    self:ScheduleMethod(20 - delay, "flameVentsTimer")
end

function mod:SPELL_SUMMON(args)
    if args:IsSpellID(62907) then        -- Ward of Life spawned (Creature id: 34275)
        warnWardofLife:Show()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(62286) and guids[args.destGUID] and guids[args.sourceGUID] == UnitName("player") then
        self:PlaySound("bochok")
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(62396) then        -- Correction of Flame Vents timer
        self:flameVentsTimer()
    elseif args:IsSpellID(62475) then    -- Systems Shutdown / Overload
        timerSystemOverload:Start()
        warnSystemOverload:Show()
        self:UnscheduleMethod("flameVentsTimer")
        self:ScheduleMethod(select(2, timerFlameVentsCD:GetTime()) - select(1, timerFlameVentsCD:GetTime()) + 20, "flameVentsTimer")
        timerFlameVentsCD:Update(select(1, timerFlameVentsCD:GetTime()), select(2, timerFlameVentsCD:GetTime()) + 20)
    elseif args:IsSpellID(62374) then    -- Pursued
        warnNextPursueSoon:Schedule(30)
        timerPursued:Start(guids[args.destGUID] or L.Unknown)
        pursueTargetWarn:Show(guids[args.destGUID] or L.Unknown)
        if guids[args.destGUID] and guids[args.destGUID] == UnitName("player") then
            specWarnPursueYou:Show()
            self:PlaySound("surprise", "fear2")
        end
    elseif args:IsSpellID(62297) then        -- Hodir's Fury (Person is frozen)
        warnHodirsFury:Show(args.destName)
        if guids[args.destGUID] and guids[args.destGUID] == UnitName("player") then
            self:PlaySound("fiasco")
        end
    elseif args:IsSpellID(68605) and args.destName == L.name then
        if timers[args.sourceGUID] then
            timers[args.sourceGUID]:UpdateName(string.format("%s (%d)", guids[args.sourceGUID] or L.Unknown, 1))
        else
            if not guids[args.sourceGUID] then
                buildGuidTable()
            end
            timers[args.sourceGUID] = self:NewTimer(10, string.format("%s (%d)", guids[args.sourceGUID] or L.Unknown, 1), 68605)
        end
        timers[args.sourceGUID]:Start()
        stacks[args.sourceGUID] = 1
        if guids[args.destGUID] and guids[args.sourceGUID] == UnitName("player") then
            warnStacksSoon:Cancel()
            warnStacksSoon:Schedule(7)
        end
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(68605) and args.destName == L.name and timers[args.sourceGUID] then
        timers[args.sourceGUID]:UpdateName(string.format("%s (%d)", guids[args.sourceGUID] or L.Unknown, args.amount))
        timers[args.sourceGUID]:Update(0.01, 10)
        stacks[args.sourceGUID] = args.amount
        if guids[args.destGUID] and guids[args.sourceGUID] == UnitName("player") then
            warnStacksSoon:Cancel()
            warnStacksSoon:Schedule(7)
        end
    end
end

function mod:SPELL_AURA_REFRESH(args)
    if args:IsSpellID(68605) and args.destName == L.name and timers[args.sourceGUID] then
        timers[args.sourceGUID]:Update(0.01, 10)
        if guids[args.destGUID] and guids[args.sourceGUID] == UnitName("player") then
            warnStacksSoon:Cancel()
            warnStacksSoon:Schedule(7)
        end
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(68605) and args.destName == L.name then
        warnStacksLost:Show(guids[args.sourceGUID] or L.Unknown, tostring(stacks[args.sourceGUID]))
        stacks[args.sourceGUID] = 0
    end
end

function mod:OnCombatEnd()
    timerFlameVentsCD:Cancel()
    self:UnscheduleMethod("flameVentsTimer")
end
