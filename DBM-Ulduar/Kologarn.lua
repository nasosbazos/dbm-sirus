﻿local mod = DBM:NewMod("Kologarn", "DBM-Ulduar")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4134 $"):sub(12, -3))
mod:SetCreatureID(32930)
mod:SetUsedIcons(5, 6, 7, 8)

mod:RegisterCombat("combat", 32930, 32933, 32934)

mod:RegisterEvents(
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REMOVED",
    "SPELL_DAMAGE",
    "CHAT_MSG_RAID_BOSS_WHISPER",
    "UNIT_DIED"
)

mod:SetBossHealthInfo(
    32930, L.Health_Body,
    32934, L.Health_Right_Arm,
    32933, L.Health_Left_Arm
)

local warnFocusedEyebeam         = mod:NewTargetAnnounce(63346, 3)
local warnGrip                   = mod:NewTargetAnnounce(64292, 2)
local warnCrunchArmor            = mod:NewTargetAnnounce(64002, 2)

local specWarnEyebeam            = mod:NewSpecialWarningYou(63346)

local yellBeam                   = mod:NewYell(63346, "SAY", L.YellBeam)

local timerGripCD                = mod:NewCDTimer(30, 64292)
local timerUntilDeath            = mod:NewTimer(15, "TimerUntilDeath", 64292)
local timerRespawnLeftArm        = mod:NewTimer(48, "timerLeftArm")
local timerRespawnRightArm       = mod:NewTimer(48, "timerRightArm")
local timerTimeForDisarmed       = mod:NewTimer(12, "achievementDisarmed")

local gripCount = 0

mod:AddBoolOption("HealthFrame", true)
mod:AddSetIconOption("SetIconOnGripTarget", 64290, {8, 7, 6}, true)
mod:AddSetIconOption("SetIconOnEyebeamTarget", 63346, {5}, true)

function mod:OnCombatStart(delay)
    gripCount = 0
    timerGripCD:Start()
end

function mod:UNIT_DIED(args)
    if self:GetCIDFromGUID(args.destGUID) == 32934 then         -- right arm
        timerRespawnRightArm:Start()
        timerGripCD:Cancel()
        timerGripCD:Schedule(48)
        timerTimeForDisarmed:Start()
    elseif self:GetCIDFromGUID(args.destGUID) == 32933 then        -- left arm
        timerRespawnLeftArm:Start()
        timerTimeForDisarmed:Start()
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(63346, 63976) and args:IsPlayer() then
        specWarnEyebeam:Show()
    end
end

function mod:CHAT_MSG_RAID_BOSS_WHISPER(msg)
    if msg:find(L.FocusedEyebeam) then
        self:SendSync("EyeBeamOn", UnitName("player"))
    end
end

function mod:OnSync(msg, target)
    if msg == "EyeBeamOn" then
        warnFocusedEyebeam:Show(target)
        if target == UnitName("player") then
            specWarnEyebeam:Show()
            self:PlaySound("fear2","run")
            yellBeam:Yell()
        end 
        if self.Options.SetIconOnEyebeamTarget then
            self:SetIcon(target, 5, 8) 
        end
    end
end

local gripTargets = {}
function mod:GripAnnounce()
    warnGrip:Show(table.concat(gripTargets, "<, >"))
    table.wipe(gripTargets)
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(64290, 64292) then
        timerUntilDeath:Start()
        timerGripCD:Start((gripCount == 1) and 47 or 30)
        gripCount = gripCount + 1
        if self.Options.SetIconOnGripTarget then
            self:SetIcon(args.destName, 8 - #gripTargets, 10)
        end
        table.insert(gripTargets, args.destName)
        self:UnscheduleMethod("GripAnnounce")
        if #gripTargets >= 3 then
            self:GripAnnounce()
        else
            self:ScheduleMethod(0.2, "GripAnnounce")
        end
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(64002) then
        if args.amount >= 2 then 
            if mod:IsTank() then
                warnCrunchArmor:Show(args.destName)
            end
        end
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(64290, 64292) then
        self:SetIcon(args.destName, 0)
    end
end