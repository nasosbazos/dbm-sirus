local mod = DBM:NewMod("Gogonash", "DBM-TolGarodPrison")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4771 $"):sub(12, -3))
mod:SetCreatureID(18464)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REMOVED",
    "SPELL_DAMAGE"
)

local isDispeller = select(2, UnitClass("player")) == "PRIEST" or select(2, UnitClass("player")) == "PALADIN" 

local warnSearedFlesh             = mod:NewAnnounce("WarnSearedFlesh", 2, 317542, mod:IsTank())
local warnFelMark                 = mod:NewTargetAnnounce(317544, 4)
local warnCrushingBlow            = mod:NewSpellAnnounce(317541, 3)

local specWarnPrimalHorror        = mod:NewSpecialWarningSpell(317548)
local specWarnFelMark             = mod:NewSpecialWarningRun(317544)
local specWarnEndlessFelFlame     = mod:NewSpecialWarningDispel(317540, isDispeller)

local yellFelMark                 = mod:NewYell(317544)

local timerPrimalHorrorCD         = mod:NewCDTimer(30, 317548)
local timerCrushingBlowCD         = mod:NewCDTimer(30, 317541, nil, mod:IsMelee())
local timerFelMarkCD              = mod:NewCDTimer(19, 317544)
local timerEndlessFelFlameCD      = mod:NewCDTimer(19, 317540, nil, isDispeller)

local markTargets = {}
local markIcon = 8

mod:AddSetIconOption("SetIconOnFelMark", 317544, {8,7}, true)

function mod:OnCombatStart(delay)
    timerFelMarkCD:Start(15-delay)
    timerPrimalHorrorCD:Start(25-delay)
    timerCrushingBlowCD:Start(-delay)
    table.wipe(markTargets)
    markIcon = 8
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(317548) then
        specWarnPrimalHorror:Show()
        timerPrimalHorrorCD:Start()
    elseif args:IsSpellID(317541) then
        warnCrushingBlow:Show()
        timerCrushingBlowCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(317540) then
        specWarnEndlessFelFlame:Show()
        timerEndlessFelFlameCD:Start()
    end
end

function mod:WarnFelMark()
    warnFelMark:Show(table.concat(markTargets, "<, >"))
    table.wipe(markTargets)
    markIcon = 8
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(317544) then
        markTargets[#markTargets + 1] = args.destName
        if self.Options.SetIconOnFelMark then
            self:SetIcon(args.destName, markIcon)
        end
        timerFelMarkCD:Start()
        if args:IsPlayer() then
            self:PlaySound("strashno")
            specWarnFelMark:Show()
            yellFelMark:Yell()
        end
        markIcon = markIcon - 1
        self:UnscheduleMethod("WarnFelMark")
        self:ScheduleMethod(0.1, "WarnFelMark")
    elseif args:IsSpellID(317542) then
        warnSearedFlesh:Show(args.spellName, args.destName, 1)
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(317542) then
        warnSearedFlesh:Show(args.spellName, args.destName, args.amount)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(317544) then
        if self.Options.SetIconOnFelMark then
            self:RemoveIcon(args.destName)
        end
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(317541) and args:IsPlayer() then
        if self:IsTank() then
            self:PlaySound("s_durak_takoi", "vnoge", "dmg")
        else
            self:PlaySound("mp_scratch", "mp_fleshwound")
        end
    end
end

function mod:OnCombatEnd(wipe)
    table.wipe(markTargets)
    markIcon = 8
end