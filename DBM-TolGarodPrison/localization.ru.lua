﻿if GetLocale() ~= "ruRU" then return end

local L

-- Gogonash
L = DBM:GetModLocalization("Gogonash")

L:SetGeneralLocalization{
    name = "Гогонаш"
}

L:SetTimerLocalization {
}

L:SetWarningLocalization{
    WarnSearedFlesh      = "%s на |3-5(>%s<) (%s)"
}

L:SetOptionLocalization{
    WarnSearedFlesh      = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.spell:format(317542, GetSpellInfo(317542) or "unknown")
}

L:SetMiscLocalization{
}

-- Mind Flayer Ktrax
L = DBM:GetModLocalization("Ktrax")

L:SetGeneralLocalization{
    name = "Поглотитель разума Ктракс"
}

L:SetTimerLocalization {
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
}
-- 
L:SetMiscLocalization{
}

-- Magic Devourer
L = DBM:GetModLocalization("MagicDevourer")

L:SetGeneralLocalization{
    name = "Пожиратель Магии"
}

L:SetTimerLocalization {
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
    RangeFrame          = "Отображать окно проверки дистанции когда на Вас\n $spell:317650 или $spell:317653"
}

L:SetMiscLocalization{
}

