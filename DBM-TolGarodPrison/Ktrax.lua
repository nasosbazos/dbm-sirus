local mod = DBM:NewMod("Ktrax", "DBM-TolGarodPrison")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4842 $"):sub(12, -3))
mod:SetCreatureID(18466)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "UNIT_HEALTH"
)

local warnBreath                = mod:NewSpellAnnounce(317571, 2)
local warnEruptingDarkness      = mod:NewSpellAnnounce(317579, 3)
local warnDarkGlobe             = mod:NewSpellAnnounce(317567, 2)
local warnPhase2                = mod:NewPhaseAnnounce(2)
local warnAncientCurse          = mod:NewTargetAnnounce(317594, 3)

local specWarnAncientCurse     = mod:NewSpecialWarningMove(317594)
local specWarnShadowWell       = mod:NewSpecialWarningMove(317595)
local specWarnDescent          = mod:NewSpecialWarningCast(317604)

local yellAncientCurse         = mod:NewYell(317594)

local timerBreathCD            = mod:NewCDTimer(26, 317571)
local timerEruptingDarknessCD  = mod:NewCDTimer(35, 317579)
local timerDarkGlobeCD         = mod:NewCDTimer(16, 317567)
local timerGiftOfYogCD         = mod:NewCDTimer(8, 317601)
local timerShadowPoolCD        = mod:NewCDTimer(35, 317596)
local timerAncientCurseCD      = mod:NewCDTimer(22, 317594)
local timerDescentCD           = mod:NewCDTimer(45, 317604)

local Phase    = 0
local curseTargets = {}
local curseIcon = 8

mod:AddSetIconOption("SetIconOnCurse", 317593, {8,7,6,5}, true)

function mod:OnCombatStart(delay)
    Phase = 1
    timerBreathCD:Start()
    timerEruptingDarknessCD:Start(23-delay)
    timerDarkGlobeCD:Start()
    table.wipe(curseTargets)
    curseIcon = 8
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(317571) then
        self:PlaySound("rightround")
        warnBreath:Show()
        timerBreathCD:Start()
    elseif args:IsSpellID(317579) then
        warnEruptingDarkness:Show()
        timerEruptingDarknessCD:Start()
    elseif args:IsSpellID(317596) then
        timerShadowPoolCD:Start()
    elseif args:IsSpellID(317604) then
        specWarnDescent:Show()
        timerDescentCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(317567) then
        warnDarkGlobe:Show()
        timerDarkGlobeCD:Start()
    end
end

function mod:WarnCurse()
    warnAncientCurse:Show(table.concat(curseTargets, "<, >"))
    table.wipe(curseTargets)
    curseIcon = 8
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(317594, 317738) then
        curseTargets[#curseTargets + 1] = args.destName
        if self.Options.SetIconOnCurse and curseIcon > 0 then
            self:SetIcon(args.destName, curseIcon, 10)
        end
        timerAncientCurseCD:Start()
        if args:IsPlayer() then
            self:PlaySound("t_zaminirovali", "s_budka")
            specWarnAncientCurse:Show()
            yellAncientCurse:Yell()
        end
        curseIcon = curseIcon - 1
        self:UnscheduleMethod("WarnCurse")
        self:ScheduleMethod(0.1, "WarnCurse")
    elseif args:IsSpellID(317595) and args:IsPlayer() and self:AntiSpam(3, "pool") then
        specWarnShadowWell:Show()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(317594) then
        if self.Options.SetIconOnCurse then
            self:RemoveIcon(args.destName)
        end
    end
end

function mod:UNIT_HEALTH(uId)
    if Phase == 1 and self:GetUnitCreatureId(uId) == 18466 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.5 then
        Phase = 2
        warnPhase2:Show()
        timerBreathCD:Start()
        timerEruptingDarknessCD:Cancel()
        timerGiftOfYogCD:Start()
        timerShadowPoolCD:Start(20)
        timerAncientCurseCD:Start()
        timerDescentCD:Start()
    end
end

function mod:OnCombatEnd()
    Phase = 0
    table.wipe(curseTargets)
    curseIcon = 8
end