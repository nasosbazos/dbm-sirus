local mod = DBM:NewMod("Maulgar", "DBM-Gruul");
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4822 $"):sub(12, -3))
mod:SetCreatureID(18831, 18832, 18834, 18835, 18836)

-- 18831 - High King Maulgar (boss)
-- 18832 - Krosh Firehand (mage)
-- 18834 - Olm the Summoner (warlock)
-- 18835 - Kiggler the Crazed (shaman)
-- 18836 - Blindeye the Seer (priest)

-- mod:RegisterCombat("yell", L.YellPull)
-- mod:RegisterKill("yell", L.YellKill)
mod:RegisterCombat("combat")
mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REMOVED",
    "UNIT_DIED",
    "SPELL_DAMAGE"
)

local isDispeller = select(2, UnitClass("player")) == "PRIEST" or select(2, UnitClass("player")) == "SHAMAN" or select(2, UnitClass("player")) == "MAGE" or select(2, UnitClass("player")) == "HUNTER"

local warnMight                     = mod:NewAnnounce("WarnMight", 2)
local warnShieldNormal              = mod:NewSpellAnnounce(33054, 3)
local warnDecay                     = mod:NewAnnounce("WarnDecay", 2, 33129, mod:IsHealer() or mod:IsTank())
local warnPolymorph                 = mod:NewTargetAnnounce(33173, 4)
local warnArcaneExplosionSoon       = mod:NewSoonAnnounce(33237, 2)
local warnFlurry                    = mod:NewSpellAnnounce(33232, 3)

local specWarnMelee                 = mod:NewSpecialWarningMove(33238, mod:IsMelee())
local specWarnShieldNormal          = mod:NewSpecialWarningDispel(33054, isDispeller)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnShield                    = mod:NewSpellAnnounce(305247, 3)
local warnLivingBomb                = mod:NewTargetAnnounce(305236, 4)
local warnDecayH                    = mod:NewAnnounce("WarnDecayH", 2, 302287, mod:IsTank())
local warnPrayerOfHealing           = mod:NewSpellAnnounce(33152, 3)
local warnInfernoSoon               = mod:NewSoonAnnounce(305238, 2)
local warnSacrificeSoon             = mod:NewSoonAnnounce(305231, 2)
local warnArcaneStormSoon           = mod:NewSoonAnnounce(305224, 2)
local warnCleanseSoon               = mod:NewSoonAnnounce(305221, 2)
local warnInferno                   = mod:NewSpellAnnounce(305238, 3)
local warnSacrifice                 = mod:NewSpellAnnounce(305231, 3)
local warnArcaneStorm               = mod:NewSpellAnnounce(305224, 3)
local warnCleanse                   = mod:NewSpellAnnounce(305221, 3)

local specWarnShield                = mod:NewSpecialWarningDispel(305247, isDispeller)
local specWarnKickCleanse           = mod:NewSpecialWarningInterupt(305221, not mod:IsMelee())
local specWarnLivingBomb            = mod:NewSpecialWarningYou(305236)

local yellLivingBomb                = mod:NewYell(305236)

local timerWhirl                    = mod:NewCastTimer(15, 33238)
local timerWhirlCD                  = mod:NewCDTimer(55, 33238)
local timerSpellShieldCD            = mod:NewCDTimer(30, 33054)
local timerSummonFelhunterCD        = mod:NewCDTimer(30, 33131)
local timerDarkDecayCD              = mod:NewCDTimer(4, 33129, nil, mod:IsTank())
local timerGreatShieldCD            = mod:NewCDTimer(40, 33147)
local timerPolymorph                = mod:NewTargetTimer(10, 33173)
local timerArcaneShockCD            = mod:NewCDTimer(20, 33175, nil, mod:IsTank())
local timerArcaneExplosionCD        = mod:NewCDTimer(30, 33237)
local timerIntimidateCD             = mod:NewCDTimer(16, 16508)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerMight                    = mod:NewTargetTimer(60, 305216, "TimerActive")
local timerMightCD                  = mod:NewCDTimer(60, 305216)
local timerDarkDecayHCD             = mod:NewCDTimer(8, 302287, nil, mod:IsTank())
local timerPrayerOfHealingCD        = mod:NewCDTimer(20, 33152)
local timerLivingBombCD             = mod:NewCDTimer(5, 305236)
local timerInferno                  = mod:NewCastTimer(50, 305238)
local timerSacrifice                = mod:NewCastTimer(50, 305231)
local timerArcaneStorm              = mod:NewCastTimer(50, 305224)
local timerCleanse                  = mod:NewCastTimer(50, 305221)

local bombTargets = {}

mod:AddBoolOption("WarnMight",true)
mod:AddBoolOption("AnnounceToChat",false)

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal25") then
        timerWhirlCD:Start(30-delay)
        timerWhirlCD:Schedule(30-delay)
        timerWhirlCD:Schedule(85-delay)
        timerDarkDecayCD:Start(-delay)
        timerSummonFelhunterCD:Start(15-delay)
        timerArcaneShockCD:Start(-delay)
        timerArcaneExplosionCD:Start(-delay)
        warnArcaneExplosionSoon:Schedule(25-delay)
    elseif self:IsDifficulty("heroic25") then
        self:PlaySound("20min")
        timerMightCD:Start(4-delay)
        timerDarkDecayHCD:Start(-delay)
        timerArcaneShockCD:Start(10-delay)
        timerArcaneExplosionCD:Start(16-delay)
        warnArcaneExplosionSoon:Schedule(11-delay)
        timerPrayerOfHealingCD:Start(40-delay)
        table.wipe(bombTargets)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(33131) then
        timerSummonFelhunterCD:Start()
    elseif args:IsSpellID(33152) then
        warnPrayerOfHealing:Show()
        timerPrayerOfHealingCD:Start()
    elseif args:IsSpellID(305221) then
        specWarnKickCleanse:Show()
    elseif args:IsSpellID(305231) then 
        self:PlaySound("resist")
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(16508) then
        timerIntimidateCD:Start()
        timerIntimidateCD:Schedule(17)
        timerIntimidateCD:Schedule(34)
    elseif args:IsSpellID(33232) then
        warnFlurry:Show()
        timerIntimidateCD:Start()
        timerIntimidateCD:Schedule(17)
        timerIntimidateCD:Schedule(34)
    elseif args:IsSpellID(33129) then
        timerDarkDecayCD:Start()
    elseif args:IsSpellID(33147) then
        timerGreatShieldCD:Start()
    elseif args:IsSpellID(33175) then
        timerArcaneShockCD:Start(self:IsDifficulty("heroic25") and 10)
    elseif args:IsSpellID(33237) then
        timerArcaneExplosionCD:Start(self:IsDifficulty("heroic25") and 16)
        warnArcaneExplosionSoon:Schedule(self:IsDifficulty("heroic25") and 11 or 25)
    elseif args:IsSpellID(302287) then
        timerDarkDecayHCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(33238) then
        timerWhirlCD:Start()
        timerWhirlCD:Schedule(55)
        timerWhirlCD:Schedule(110)
        timerWhirl:Start()
        specWarnMelee:Show()
    elseif args:IsSpellID(33054) and self:GetCIDFromGUID(args.destGUID) == 18832 then
        warnShieldNormal:Show()
        timerSpellShieldCD:Start()
        if UnitName("target") == args.destName then
            specWarnShieldNormal:Show(args.destName)
        end
    elseif args:IsSpellID(33129) then
        warnDecay:Show(args.spellName, args.destName, 1)
    elseif args:IsSpellID(33054) then
        warnPolymorph:Show(args.destName)
        timerPolymorph:Start(args.destName)
    elseif args:IsSpellID(305216) then
        local name, uId = self:GetBossTarget(18831)
        local activeIcon = uId and GetRaidTargetIndex(uId)
        warnMight:Show(args.destName, activeIcon and ("rt" .. activeIcon))
        timerMight:Start(args.destName, activeIcon and ("rt" .. activeIcon))
        if self.Options.AnnounceToChat and activeIcon then
            SendChatMessage((activeIcon and ("{rt" .. activeIcon .. "} ") or "") .. args.destName .. L.AnnounceActive, "RAID")
        end
        local cid = self:GetCIDFromGUID(args.destGUID)
        if     cid == 18832 then
            timerLivingBombCD:Start()
            timerInferno:Start()
            warnInfernoSoon:Schedule(45)
        elseif cid == 18834 then
            timerSacrifice:Start()
            warnDecayH:Cancel()
            warnSacrificeSoon:Schedule(45)
        elseif cid == 18835 then
            timerArcaneShockCD:Cancel()
            timerArcaneExplosionCD:Cancel()
            warnArcaneExplosionSoon:Cancel()
            timerArcaneStorm:Start()
            warnArcaneStormSoon:Schedule(45)
        elseif cid == 18836 then
            timerCleanse:Start()
            warnCleanseSoon:Schedule(45)
            timerPrayerOfHealingCD:Cancel()
        end
    elseif args:IsSpellID(305247) then
        warnShield:Show()
        if UnitName("target") == args.destName then
            specWarnShield:Show(args.destName)
        end
    elseif args:IsSpellID(302287) then
        warnDecayH:Show(args.spellName, args.destName, 1)
    elseif args:IsSpellID(305236) then
        bombTargets[#bombTargets + 1] = args.destName
        if #bombTargets >= 3 then 
            warnLivingBomb:Show(table.concat(bombTargets, "<, >"))
            table.wipe(bombTargets)
        end
        if args:IsPlayer() then
            specWarnLivingBomb:Show()
            yellLivingBomb:Yell()
        end
        timerLivingBombCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(33129) then
        warnDecay:Show(args.spellName, args.destName, args.amount)
    elseif args:IsSpellID(302287) then
        warnDecayH:Show(args.spellName, args.destName, args.amount)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(305216) then
        local cid = self:GetCIDFromGUID(args.destGUID)
        if     cid == 18832 then
            timerLivingBombCD:Cancel()
        elseif cid == 18834 then
            timerDarkDecayHCD:Start()
        elseif cid == 18835 then
            timerArcaneShockCD:Start(10)
            timerArcaneExplosionCD:Start(16)
            warnArcaneExplosionSoon:Schedule(11)
        elseif cid == 18836 then
            timerPrayerOfHealingCD:Start()
        end
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(33051) and args:IsPlayer() and args.overkill > 0 then
        self:PlaySound("dmg", "fiasco", "resist")
    elseif args:IsSpellID(33237) and args:IsPlayer() then
        self:PlaySound("ebany_rot", "dosvidania")
    end
end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if     cid == 18832 then
        timerSpellShieldCD:Cancel()
        timerLivingBombCD:Cancel()
        timerInferno:Cancel()
        warnInfernoSoon:Cancel()
    elseif cid == 18834 then
        timerSummonFelhunterCD:Cancel()
        timerDarkDecayCD:Cancel()
        timerDarkDecayHCD:Cancel()
        timerSacrifice:Cancel()
        warnSacrificeSoon:Cancel()
    elseif cid == 18835 then
        timerArcaneShockCD:Cancel()
        timerArcaneExplosionCD:Cancel()
        warnArcaneExplosionSoon:Cancel()
        timerArcaneStorm:Cancel()
        warnArcaneStormSoon:Cancel()
    elseif cid == 18836 then
        timerGreatShieldCD:Cancel()
        timerCleanse:Cancel()
        warnCleanseSoon:Cancel()
    end
end
