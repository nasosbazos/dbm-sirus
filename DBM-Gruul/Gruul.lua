local mod = DBM:NewMod("Gruul", "DBM-Gruul");
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4858 $"):sub(12, -3))
mod:SetCreatureID(19044)

mod:RegisterCombat("combat")
mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_DAMAGE",
    "SPELL_MISSED"
)

local warnEcho                  = mod:NewSpellAnnounce(36297, 2)
local warnGroundSlam            = mod:NewSpellAnnounce(33525, 3)
local warnGroundSlamSoon        = mod:NewSoonAnnounce(33525, 3)

local specWarnRock              = mod:NewSpecialWarningMove(36240)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnRockCall              = mod:NewSpellAnnounce(305188, 3)
local warnBurnedFlesh           = mod:NewSpellAnnounce(305204, 2)
local warnFireShard             = mod:NewSpellAnnounce(305181, 4)
local warnDashing               = mod:NewAnnounce("WarnDashing", 2, 305183)

local specWarnBurnedFlesh       = mod:NewSpecialWarning("SpecWarnBurnedFlesh", false)
local specWarnFire              = mod:NewSpecialWarningMove(305182)

local timerGroundSlamCD         = mod:NewCDTimer(133, 33525)
local timerEchoCD               = mod:NewCDTimer(18, 36297)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerRockCallCD           = mod:NewCDTimer(30, 305188)
local timerDashingStrikeCD      = mod:NewCDTimer(15, 305183)
local timerFireShardCD          = mod:NewCDTimer(80, 305181)
local timerFire                 = mod:NewBuffActiveTimer(40, 305182)
local timerHellishStrike        = mod:NewCastTimer(9, 305185)
local timerRockCall             = mod:NewTimer(6,"TimerRockCall", 305188)
local timerFurnaceActive        = mod:NewTimer(8,"TimerFurnaceActive", 305201)
local timerFurnaceInactive      = mod:NewTimer(43,"TimerFurnaceInactive", 305201)
local timerBurnedFlesh          = mod:NewTimer(20,"TimerBurnedFlesh", 305204)
local berserkTimer              = mod:NewBerserkTimer(600)

local dashingTargets = {}

function mod:FireShard()
    warnFireShard:Show()
    timerFireShardCD:Start()
    timerFire:Schedule(5)
    self:UnscheduleMethod("FireShard")
    self:ScheduleMethod(80, "FireShard")
end

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal25") then
        timerGroundSlamCD:Start(35-delay)
        warnGroundSlamSoon:Schedule(30-delay)
    elseif self:IsDifficulty("heroic25") then
        timerRockCallCD:Start(25-delay)
        timerDashingStrikeCD:Start(-delay)
        timerFireShardCD:Start(-delay)
        self:ScheduleMethod(80-delay, "FireShard")
        berserkTimer:Start(-delay)
        table.wipe(dashingTargets)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(33525) then
        self:ScheduleMethod(10, "PlaySound", "suffer_bitch")
        warnGroundSlam:Show()
        timerGroundSlamCD:Start()
        warnGroundSlamSoon:Schedule(128)
        timerEchoCD:Start(25)
    elseif args:IsSpellID(305183) then
        timerDashingStrikeCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(305188) then
        warnRockCall:Show()
        timerRockCallCD:Start()
        timerRockCall:Start()
    elseif args:IsSpellID(305180) then
        timerFireShardCD:Start()
        timerFire:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(305201) then
        timerFurnaceActive:Start()
        timerHellishStrike:Start()
        if self:IsMelee() then
            self:PlaySound("b_nelez")
        end
    elseif args:IsSpellID(305204) then
        warnBurnedFlesh:Show()
        specWarnBurnedFlesh:Show()
        timerBurnedFlesh:Start()
    elseif args:IsSpellID(36297) and self:AntiSpam(2, "echo") then
        warnEcho:Show()
        timerEchoCD:Start()
    elseif args:IsSpellID(36240) and args:IsPlayer() then
        specWarnRock:Show()
    elseif args:IsSpellID(305182) and args:IsPlayer() and self:AntiSpam(5, "fire") then
        specWarnFire:Show()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(305201) then
        timerFurnaceInactive:Start()
    end
end

function mod:WarnDashing()
    warnDashing:Show(#dashingTargets ,table.concat(dashingTargets, "<, >"))
    table.wipe(dashingTargets)
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(305183) and args:IsDestTypePlayer() then
        dashingTargets[#dashingTargets + 1] = args.destName
        self:UnscheduleMethod("WarnDashing")
        self:ScheduleMethod(0.1, "WarnDashing")
    end
end

mod.SPELL_MISSED = mod.SPELL_DAMAGE
